if Flag_1==1 % ((2*pi*Rin-magnet_num*wm)/magnet_num)<0
    display('Construction limitation: It is not possible to build this magnet rotor because the magnets are overlapping at the inner radius of the generator. Try reducing the number of poles.')
end

if Flag_2==1 % (coil_hole_Rin_constr-5)<=0
    display('Construction limitation: It is not possible to build this coil because its whole at the inner radius is less than 5mm. Try reducing the number of poles.')
end

if Flag_3==1 % coil_spacing<coil_side_dist
    display('Construction limitation: It is not possible to build this stator because the coils are overlapping at the average radius of the generator. Try increasing the number of poles.')
end

if Flag_4==1 % (tw/wc)<=0.25
    display('Construction limitation: It is not possible to build this stator because the leg width of the coils is more than three times the thickness of the stator. Try increasing the number of poles or the value of the current density.')
end

if Flag_5==1 % (tw/wc)>2.5
    display('Construction limitation: It is not possible to build this stator because the thickness of the coils is more than twice the leg width of the coils. Try reducing the number of poles.')
end

if Flag_6==1 % efficiency_real<0.65
    display('Warning: The efficiency of the generator is below 65% and it is advisable not to reduce it further. Consider increasing the number of poles or reducing the current density.')
end

if Flag_7==1 % cq_real>0.4
    display('Warning: The heat coefficient is more than 0.4 and this will create temperatures in the stator of above 115 degrees Celsius. Consider increasing the number of poles or reducing the current density.')
end

if Flag_8==1 % Jmax_real>6.5
    display('Warning: The current density is more than 6.5 and this will create high temperatures in the stator. Consider increasing the number of poles.')
end

if Flag_9==1 % sc_multi_wires==2.84
    display('Construction limitation: It is not possible to build this stator because the coils require more than 8 wires at hand of 1.9mm copper wire diameter. Consider increasing the number of poles or the system voltage.')
end

if Flag_10==1 % break_counter==20
    display('Construction limitation: It was not possible to design this generator after trying 20 times. Look carefully at your design.')
end
