 % select conductor from commercialy availabel sizes

if sc_theor<=2.83
 
No_wires_at_hand=1; % one wire at hand
dc_theor=sqrt(4*sc_theor/3.14);

if sc_theor<=0.4
        sc=0.4; % mm2
        dc=0.71; % mm
    elseif ((sc_theor>0.4)&&(sc_theor<=0.44))
        sc=0.44; % mm2
        dc=0.75; % mm
    elseif ((sc_theor>0.44)&&(sc_theor<=0.5))
        sc=0.5; % mm2
        dc=0.8; % mm
    elseif ((sc_theor>0.5)&&(sc_theor<=0.57))
        sc=0.57; % mm2
        dc=0.85; % mm
    elseif ((sc_theor>0.57)&&(sc_theor<=0.64))
        sc=0.64; % mm2
        dc=0.9; % mm
    elseif ((sc_theor>0.64)&&(sc_theor<=0.71))
        sc=0.71; % mm2
        dc=0.95; % mm  
    elseif ((sc_theor>0.71)&&(sc_theor<=0.79))
        sc=0.79; % mm2
        dc=1; % mm       
    elseif ((sc_theor>0.79)&&(sc_theor<=0.88))
        sc=0.88; % mm2
        dc=1.06; % mm
    elseif ((sc_theor>0.88)&&(sc_theor<=0.99))
        sc=0.99; % mm2
        dc=1.12; % mm
    elseif ((sc_theor>0.99)&&(sc_theor<=1.09))
        sc=1.09; % mm2
        dc=1.18; % mm
    elseif ((sc_theor>1.09)&&(sc_theor<=1.23))
        sc=1.23; % mm2
        dc=1.25; % mm
    elseif ((sc_theor>1.23)&&(sc_theor<=1.37))
        sc=1.37; % mm2
        dc=1.32; % mm
    elseif ((sc_theor>1.37)&&(sc_theor<=1.54))
        sc=1.54; % mm2
        dc=1.40; % mm
    elseif ((sc_theor>1.54)&&(sc_theor<=1.77))
        sc=1.77; % mm2
        dc=1.5; % mm
    elseif ((sc_theor>1.77)&&(sc_theor<=2.01))
        sc=2.01; % mm2
        dc=1.6; % mm
    elseif ((sc_theor>2.01)&&(sc_theor<=2.27))
        sc=2.27; % mm2
        dc=1.7; % mm
    elseif ((sc_theor>2.27)&&(sc_theor<=2.54))
        sc=2.54; % mm2
        dc=1.8; % mm
    elseif ((sc_theor>2.54)&&(sc_theor<=2.83))
        sc=2.83; % mm2
        dc=1.9; % mm
end

sc_multi_wires=sc;

end

if sc_theor>2.83
    dc_theor=sqrt(4*sc_theor/3.14);
    
    for No_wires_at_hand=2:1:8 % how many wires at hand are needed?
        sc_multi_wires_theor=sc_theor/No_wires_at_hand;
        if  sc_multi_wires_theor<=2.54
            break
        else
        end
    end
    
    if sc_multi_wires_theor<=0.4
        sc_multi_wires=0.4; % mm2
        dc=0.71; % mm
    elseif ((sc_multi_wires_theor>0.4)&&(sc_multi_wires_theor<=0.44))
        sc_multi_wires=0.44; % mm2
        dc=0.75; % mm
    elseif ((sc_multi_wires_theor>0.44)&&(sc_multi_wires_theor<=0.5))
        sc_multi_wires=0.5; % mm2
        dc=0.8; % mm
    elseif ((sc_multi_wires_theor>0.5)&&(sc_multi_wires_theor<=0.57))
        sc_multi_wires=0.57; % mm2
        dc=0.85; % mm
    elseif ((sc_multi_wires_theor>0.57)&&(sc_multi_wires_theor<=0.64))
        sc_multi_wires=0.64; % mm2
        dc=0.9; % mm
    elseif ((sc_multi_wires_theor>0.64)&&(sc_multi_wires_theor<=0.71))
        sc_multi_wires=0.71; % mm2
        dc=0.95; % mm  
    elseif ((sc_multi_wires_theor>0.71)&&(sc_multi_wires_theor<=0.79))
        sc_multi_wires=0.79; % mm2
        dc=1; % mm       
    elseif ((sc_multi_wires_theor>0.79)&&(sc_multi_wires_theor<=0.88))
        sc_multi_wires=0.88; % mm2
        dc=1.06; % mm
    elseif ((sc_multi_wires_theor>0.88)&&(sc_multi_wires_theor<=0.99))
        sc_multi_wires=0.99; % mm2
        dc=1.12; % mm
    elseif ((sc_multi_wires_theor>0.99)&&(sc_multi_wires_theor<=1.09))
        sc_multi_wires=1.09; % mm2
        dc=1.18; % mm
    elseif ((sc_multi_wires_theor>1.09)&&(sc_multi_wires_theor<=1.23))
        sc_multi_wires=1.23; % mm2
        dc=1.25; % mm
    elseif ((sc_multi_wires_theor>1.23)&&(sc_multi_wires_theor<=1.37))
        sc_multi_wires=1.37; % mm2
        dc=1.32; % mm
    elseif ((sc_multi_wires_theor>1.37)&&(sc_multi_wires_theor<=1.54))
        sc_multi_wires=1.54; % mm2
        dc=1.40; % mm
    elseif ((sc_multi_wires_theor>1.54)&&(sc_multi_wires_theor<=1.77))
        sc_multi_wires=1.77; % mm2
        dc=1.5; % mm
    elseif ((sc_multi_wires_theor>1.77)&&(sc_multi_wires_theor<=2.01))
        sc_multi_wires=2.01; % mm2
        dc=1.6; % mm
    elseif ((sc_multi_wires_theor>2.01)&&(sc_multi_wires_theor<=2.27))
        sc_multi_wires=2.27; % mm2
        dc=1.7; % mm
    elseif ((sc_multi_wires_theor>2.27)&&(sc_multi_wires_theor<=2.54))
        sc_multi_wires=2.54; % mm2
        dc=1.8; % mm
    elseif ((sc_multi_wires_theor>2.54)&&(sc_multi_wires_theor<=2.83))
        sc_multi_wires=2.83; % mm2
        dc=1.9; % mm
    elseif No_wires_at_hand>=8 && sc_multi_wires_theor>2.83
        sc_multi_wires=2.84;
        dc=1.9; % mm
    end
    sc=sc_multi_wires*No_wires_at_hand;
end
