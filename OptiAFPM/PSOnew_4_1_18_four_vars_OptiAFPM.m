clear d S v dGmin dmin Gmin input k Smin

global Flag_1 Flag_2 Flag_3 Flag_4 Flag_5 Flag_6 Flag_7 Flag_8 Flag_9 Flag_10 winder_bolt_size location_path Jmax_real cq_real V_batt_nom design_tsr p20 Penalty Eff_weigth Cost_weight Mass_weight location_path rotor coil_type mag_mater cq_user g Rturb tsr_cut_in tsr_nom Vw_cut_in Vw_nom cp V_batt air_dens trans_loss rect_loss poles hr pFe pcu copperprice ironprice resinprice presin pplywood mag_mater efficiency_real Flux_pole Jmax Bp Phl fnom tw mcu efficiency Magcost output TotalMass cp Totalcost poles q Hc  Bmg ai hm g kd kf Rout Rin la Ravg kw Nc wc dc wm Ls Rturb ;

% parameters setup
n_particle=5; % numbers of particles
Vmax=[1 1 1 1]; % velocity band
inertia=[0.7 0.7 0.7 0.7]; % inertia constant
phi1=[0.3 0.3 0.3 0.3]; % self acceleration constant
phi2=[0.7 0.7 0.7 0.7]; % social acceleration constant
%iteration_max=25; % maximum iterations
%n_variable=4; % number of design variables

 % initialization
% d=10000*ones(round(n_variable*n_particle),round(iteration_max));
d(:,1)=[(la_max-la_min)*rand(n_particle,1)+la_min; (wm_max-wm_min)*rand(n_particle,1)+wm_min; (hm_max-hm_min)*rand(n_particle,1)+hm_min; (tw_max-tw_min)*rand(n_particle,1)+tw_min];

for k=1:n_particle
input=[d(k,1) d(k+n_particle,1) d(k+2*n_particle,1) d(k+3*n_particle,1)];
S(k,1)=functiondiastnew_4_1_18_four_vars_OptiAFPM(input);
end

% for k=1:n_particle
% input=[d(k,1) d(k+n_particle,1) d(k+2*n_particle,1)
%d(k+3*n_particle,1)];
% S(k,1)=TheoDesignFunDist_4input_v6(input);
%
% end

Smin=S; %minimum for each particle
dmin=d; %optimum position for each particle

v(1:n_particle,2)=2*Vmax(1)*(rand(n_particle,1)-0.5); %initialize
%velocity for DiaSGap
v(n_particle+1:n_particle*2,2)=2*Vmax(2)*(rand(n_particle,1)-0.5); %initialize velocity for Length
v(n_particle*2+1:n_particle*3,2)=2*Vmax(3)*(rand(n_particle,1)-0.5); %initialize velocity for ThickMag
v(n_particle*3+1:n_particle*4,2)=2*Vmax(4)*(rand(n_particle,1)-0.5); %initialize velocity for Current density

[Gmin i]=min(Smin); %global minimum
dGmin=[d(i) d(i+n_particle) d(i+2*n_particle) d(i+3*n_particle)]; %global optimum position
% Searching

tic

for i=2:iteration_max;
d(:,i)=d(:,i-1)+v(:,i);
%S(:,i)=exp(d(:,i)).*sin(2*pi*d(:,i))+5e2*(2+sign(d(:,i)-5)-
%sign(d(:,i)));

for k=1:n_particle % use for when calling FEMMM and parfor for rest
input=[d(k,i) d(k+n_particle,i) d(k+2*n_particle) d(k+3*n_particle)];
S(k,i)=functiondiastnew_4_1_18_four_vars_OptiAFPM(input);
end

%update local minimum and position
for k=1:n_particle
if Smin(k)>S(k,i)
Smin(k)=S(k,i);
dmin(k)=d(k,i);
dmin(k+n_particle)=d(k+n_particle,i);
dmin(k+n_particle*2)=d(k+n_particle*2,i);
dmin(k+n_particle*3)=d(k+n_particle*3,i);
end;
end;

%update global minimum and position
if Gmin>min(Smin)
[Gmin j]=min(Smin);
dGmin(1)=dmin(j);
dGmin(2)=dmin(j+n_particle);
dGmin(3)=dmin(j+n_particle*2);
dGmin(4)=dmin(j+n_particle*3);
end

%update velocity
v(1:n_particle,i+1)=inertia(1)*v(1:n_particle,i)+phi1(1)*rand*(dmin(1:n_particle)-d(1:n_particle,i))+phi2(1)*rand*(dGmin(1)-d(1:n_particle,i));

v(n_particle+1:n_particle*2,i+1)=inertia(2)*v(n_particle+1:n_particle*2,i)+phi1(2)*rand*(dmin(n_particle+1:n_particle*2)-d(n_particle+1:n_particle*2,i))+phi2(2)*rand*(dGmin(2)-d(n_particle+1:n_particle*2,i));

v(n_particle*2+1:n_particle*3,i+1)=inertia(3)*v(n_particle*2+1:n_particle*3,i)+phi1(3)*rand*(dmin(n_particle*2+1:n_particle*3)-d(n_particle*2+1:n_particle*3,i))+phi2(3)*rand*(dGmin(3)-d(n_particle*2+1:n_particle*3,i));

v(n_particle*3+1:n_particle*4,i+1)=inertia(4)*v(n_particle*3+1:n_particle*4,i)+phi1(4)*rand*(dmin(n_particle*3+1:n_particle*4)-d(n_particle*3+1:n_particle*4,i))+phi2(4)*rand*(dGmin(4)-d(n_particle*3+1:n_particle*4,i));

%band of velocity
v(1:n_particle,i+1)=min(v(1:n_particle,i+1),Vmax(1));
v(1:n_particle,i+1)=max(v(1:n_particle,i+1),-Vmax(1));

v(n_particle+1:n_particle*2,i+1)=min(v(n_particle+1:n_particle*2,i+1),Vmax(2));
v(n_particle+1:n_particle*2,i+1)=max(v(n_particle+1:n_particle*2,i+1),-Vmax(2));

v(n_particle*2+1:n_particle*3,i+1)=min(v(n_particle*2+1:n_particle*3,i+1),Vmax(3));
v(n_particle*2+1:n_particle*3,i+1)=max(v(n_particle*2+1:n_particle*3,i+1),-Vmax(3));

v(n_particle*3+1:n_particle*4,i+1)=min(v(n_particle*3+1:n_particle*4,i+1),Vmax(4));
v(n_particle*3+1:n_particle*4,i+1)=max(v(n_particle*3+1:n_particle*4,i+1),-Vmax(4));
end;

dGmin;
Gmin;
toc



% Plot
t=1:iteration_max;
figure(1)
for n=2:n_particle
plot(t,d(n,:));
hold on;
end

for n=n_particle+1:n_particle*2
plot(t,d(n,:),'r');
hold on;
end

for n=n_particle*2+1:n_particle*3
plot(t,d(n,:),'g');
hold on;
end

for n=n_particle*3+1:n_particle*4
plot(t,d(n,:),'y');
hold on;
end


hold off
xlabel('Number of iterations')
ylabel('Values of la (blue), wm (red), hm (green), tw (yellow)')
title('Particle behavior')

figure(2)
for n=1:n_particle
plot(t,S(n,:));
hold on;
end
hold off
xlabel('Number of iterations')

if Cost_weight==0 && Mass_weight==0
   ylabel('Generator Efficiency') % maximise efficiency
elseif Eff_weigth==0 && Mass_weight==0
   ylabel('Generator Cost (EUR)') % minimise cost
elseif Eff_weigth==0 && Cost_weight==0
   ylabel('Generator Mass (kg)') % minimise mass
else
    ylabel('Combined output') % combined
end

title('Output of each particle')

% Result of last itteration
Particle_Output=0;
Cost_temp=0;
Eff_temp=0;
Blue=0;
Red=0;
Green=0;
Yellow=0;

for t=1:iteration_max
for n=1:n_particle
    if t==iteration_max
        if Cost_weight==0 && Mass_weight==0
            Eff_temp=1-S(n,t)/(Eff_weigth*200);
            Particle_Output=Particle_Output+Eff_temp;
        elseif Eff_weigth==0 && Mass_weight==0
            Cost_temp=S(n,t)*10/Cost_weight;
            Particle_Output=Particle_Output+Cost_temp;
        else
            Particle_Output=Particle_Output+S(n,t);
        end
    end
end

if Particle_Output>0
    Particle_Output=Particle_Output/n_particle;
end

for n=1:n_particle
    if t==iteration_max
        Blue=Blue+d(n,t);
    end
end

if Blue>0
    Blue=Blue/n_particle;
end

for n=n_particle+1:n_particle*2
    if t==iteration_max
        Red=Red+d(n,t);
    end
end

if Red>0
    Red=Red/n_particle;
end

for n=n_particle*2+1:n_particle*3
    if t==iteration_max
        Green=Green+d(n,t);
    end
end

if Green>0
    Green=Green/n_particle;
end

for n=n_particle*3+1:n_particle*4
    if t==iteration_max
        Yellow=Yellow+d(n,t);
    end
end

if Yellow>0
    Yellow=Yellow/n_particle;
end
end
