clear all
close all

global Flag_1 Flag_2 Flag_3 Flag_4 Flag_5 Flag_6 Flag_7 Flag_8 Flag_9 Flag_10
global winder_bolt_size Jmax_real cq_real V_batt_nom design_tsr p20 Penalty
global Eff_weigth Cost_weight Mass_weight location_path rotor coil_type
global mag_mater cq_user g Rturb tsr_cut_in tsr_nom Vw_cut_in Vw_nom cp V_batt
global air_dens trans_loss rect_loss poles hr pFe pcu copperprice ironprice
global resinprice presin pplywood mag_mater efficiency_real Flux_pole Jmax Bp
global Phl fnom tw mcu efficiency Magcost output TotalMass cp Totalcost poles q
global Hc  Bmg ai hm g kd kf Rout Rin la Ravg kw Nc wc dc wm Ls Rturb ;

% Generator input parameters
addpath('c:\femm42\mfiles'); % add FEMM mfiles to MATLAB
location_path=['C:\AFPM_mfiles\OpenAFPM\OpenAFPM_3_1_18\OptiAFPM\']; % path location to write files and read from

% choose between single or double rotor topology
rotor=2; % double rotor (2) or single rotor with metal disk (1) or single rotor (0)

% Choose stator winding type
coil_type=1; % choose (1) for rectangular coils or (2) for keyhole coils or (3) for triangular coils

if coil_type==3
    winder_bolt_size=8; % coil winder bolt size at Rin for trianular coils (type 3)
end

% magnetic material http://www.ndfeb-info.com/neodymium_grades.aspx
mag_mater='NdFeB N40'; % choose magnetic material between 'NdFeB N40/N42/N45' and 'Ferrite C8'

Jmax=6; % rated current density
% mechanical clearence + effective length + magnet size
g=3; % mechanical clearence gap including resin layers 0.5mm on coil and 0.5mm on magnet
% blade rotor
Rturb=1.2; % turbine radius in meters
tsr_cut_in=8.75; % cut in tip speed ratio
Vw_cut_in=3; % cut in wind speed
Vw_nom=10; % rated-nominal windspeed
cp=0.3; % aerodynamic power coefficient
design_tsr=7; % design tsr - optimal tip speed ratio for blades (between 6 and 7) use it for MPPT

% battery or wind inverter cutin
V_batt=24; % battery voltage or lower part of VDC range of wind inverter
% battery or wind inverter nominal voltage
V_batt_nom=24; % battery voltage at nominal wind speed (this can be system voltage or charge controller dump voltage)

% rated power
air_dens=1.204; % air density at 20 degrees
trans_loss=10; % Percent losses on cables at 10m/s
rect_loss=10; % Percent losses on rectifier
poles=12; % number of poles
hr=10; % 'hr' rotor disk thickness

pFe=7.87; % density of iron g/cm3
pcu=8.94; % copper density g/cm3
copperprice=12; % in Euro/kg
p20=1.678*10^(-8); % specific resistance of copper at 20 deg
ironprice=1; % in Euro/kg
resinprice=11.23; % in Euro/kg
presin=1.36; % in g/cm3
pplywood=17.6; % Euro/m2 for 12mm

% PSO parameter setup
Eff_weigth=1; % 'Weight for Efficiency' - Values from 0 to 1 or more with 1 given full importance and 0 not considered
Cost_weight=1; % 'Weight for Cost' - Values from 0 to 1 or more with 1 given full importance and 0 not considered
Mass_weight=2; % 'Weight for Mass' - Values from 0 to 1 or more with 1 given full importance and 0 not considered
n_variable=4; % 'Number of design variables' choose between 2, 3 or 4
iteration_max=20; % 'Number of particle generations' Provide some maximum here eg 40
no_loops=1; % 'Times to repeat PSO' times to loop the process  max 15

% Give min and max values for design variables
la_max=70;
la_min=50;
wm_max=40;
wm_min=20;

openfemm; % open FEMM

if n_variable==2
    hm=10;
    tw=13;
    for loop=1:no_loops
        PSOnew_4_1_18_two_vars_OptiAFPM;
        la_out(1,loop)=Blue;
        wm_out(1,loop)=Red;
        Particle_Output_out(1,loop)=Particle_Output;
        Total_Mass_out(1,loop)=TotalMass;
        Total_Cost_out(1,loop)=Totalcost;
        Efficiency_Min_out(1,loop)=efficiency_real;
        ObjFun_Eff_term(1,loop)=Eff_weigth*(1-efficiency_real)*200;
        ObjFun_Cost_term(1,loop)=Cost_weight*Totalcost/10;
        ObjFun_Mass_term(1,loop)=Mass_weight*TotalMass;
        ObjFun_Penalty_term(1,loop)=Penalty;
        error_messages;
    end
end

if n_variable==3
    hm_max=15;
    hm_min=8;
    tw=13;
    for loop=1:no_loops
        PSOnew_4_1_18_three_vars_OptiAFPM;
        la_out(1,loop)=Blue;
        wm_out(1,loop)=Red;
        hm_out(1,loop)=Green;
        Particle_Output_out(1,loop)=Particle_Output;
        Total_Mass_out(1,loop)=TotalMass;
        Total_Cost_out(1,loop)=Totalcost;
        Efficiency_Min_out(1,loop)=efficiency_real;
        ObjFun_Eff_term(1,loop)=Eff_weigth*(1-efficiency_real)*200;
        ObjFun_Cost_term(1,loop)=Cost_weight*Totalcost/10;
        ObjFun_Mass_term(1,loop)=Mass_weight*TotalMass;
        ObjFun_Penalty_term(1,loop)=Penalty;
        error_messages;
    end
end

if n_variable==4
    hm_max=15;
    hm_min=8;
    tw_max=18;
    tw_min=10;
    for loop=1:no_loops
        PSOnew_4_1_18_four_vars_OptiAFPM;
        la_out(1,loop)=Blue;
        wm_out(1,loop)=Red;
        hm_out(1,loop)=Green;
        tw_out(1,loop)=Yellow;
        Particle_Output_out(1,loop)=Particle_Output;
        Total_Mass_out(1,loop)=TotalMass;
        Total_Cost_out(1,loop)=Totalcost;
        Efficiency_Min_out(1,loop)=efficiency_real;
        ObjFun_Eff_term(1,loop)=Eff_weigth*(1-efficiency_real)*200;
        ObjFun_Cost_term(1,loop)=Cost_weight*Totalcost/10;
        ObjFun_Mass_term(1,loop)=Mass_weight*TotalMass;
        ObjFun_Penalty_term(1,loop)=Penalty;
        error_messages;
    end
end

display 'Simulation complete'
