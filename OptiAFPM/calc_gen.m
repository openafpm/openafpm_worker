%%%%%%%%%%%%% environment setup %%%%%%%%%%%%%

addpath('~/.wine/drive_c/femm42/mfiles'); % add FEMM mfiles to MATLAB

global Flag_1 Flag_2 Flag_3 Flag_4 Flag_5 Flag_6 Flag_7 Flag_8 Flag_9 Flag_10
global winder_bolt_size Jmax_real cq_real V_batt_nom design_tsr p20 Penalty
global Eff_weigth Cost_weight Mass_weight location_path rotor coil_type
global mag_mater cq_user g Rturb tsr_cut_in tsr_nom Vw_cut_in Vw_nom cp V_batt
global air_dens trans_loss rect_loss poles hr pFe pcu copperprice ironprice
global resinprice presin pplywood mag_mater efficiency_real Flux_pole Jmax Bp
global Phl fnom tw mcu efficiency Magcost output TotalMass cp Totalcost poles q
global Hc  Bmg ai hm g kd kf Rout Rin la Ravg kw Nc wc dc wm Ls Rturb ;

input_vars

% Unused variables to be removed
unused_vars = ['addition'
'angle'
'ans'
'Bavg'
'BBB'
'Joule'
'm3'
'Bmavg_real'
'BMAX'
'Bmax_real'
'BMG'
'Bmg_real'
'Bp_real'
'Bp_real'
'Bpmax'
'break_counter'
'Brms'
'coil_FEMM'
'coil_percent'
'coil_side_dist'
'coil_spacing'
'coilangle_Rin'
'coilangle_Rout'
'counter'
'counter_energy'
'delta'
'deltaN'
'depth'
'displacement'
'Efficiency'
'extra'
'f'
'Flag'
'Flux_pole_BOOK'
'Fluxoc'
'Fs'
'g_init'
'i'
'Imax'
'j'
'k'
'L_half'
'lavgHP'
'lec'
'mag_dir'
'magnet_FEMM'
'msize'
'Nc_init'
'NFFT'
'Overall_Energy_per_Volume'
'paragogost'
'path'
'Phl'
'Plosses'
'Q'
'Radius'
'ResHP_20'
'ResHP_70'
'Rin_coil_spacing'
'Rin_init'
'rts'
'segment'
'SIZE'
'Speed'
'sumA'
'sumB'
'sumC'
'sumsquareA'
'sumsquareB'
'sumsquareC'
'temp_var'
'tempvar'
'vector_Force'
'vectorA'
'vectorB'
'vectorC'
'wc_theor'
'x'
'xx'
'y'
'Y'
'ypol'
'ypol_2'
'ypol_3'
'yy',
'mag_mater',
'location_path',
'unused_vars'];

openfemm; % open FEMM

% run sim
if n_variable==2
    hm=10;
    tw=13;
    for loop=1:no_loops
        PSOnew_4_1_18_two_vars_OptiAFPM;
        la_out(1,loop)=Blue;
        wm_out(1,loop)=Red;
        Particle_Output_out(1,loop)=Particle_Output;
        Total_Mass_out(1,loop)=TotalMass;
        Total_Cost_out(1,loop)=Totalcost;
        Efficiency_Min_out(1,loop)=efficiency_real;
        ObjFun_Eff_term(1,loop)=Eff_weigth*(1-efficiency_real)*200;
        ObjFun_Cost_term(1,loop)=Cost_weight*Totalcost/10;
        ObjFun_Mass_term(1,loop)=Mass_weight*TotalMass;
        ObjFun_Penalty_term(1,loop)=Penalty;
        error_messages;
    end
end

if n_variable==3
    hm_max=15;
    hm_min=8;
    tw=13;
    for loop=1:no_loops
        PSOnew_4_1_18_three_vars_OptiAFPM;
        la_out(1,loop)=Blue;
        wm_out(1,loop)=Red;
        hm_out(1,loop)=Green;
        Particle_Output_out(1,loop)=Particle_Output;
        Total_Mass_out(1,loop)=TotalMass;
        Total_Cost_out(1,loop)=Totalcost;
        Efficiency_Min_out(1,loop)=efficiency_real;
        ObjFun_Eff_term(1,loop)=Eff_weigth*(1-efficiency_real)*200;
        ObjFun_Cost_term(1,loop)=Cost_weight*Totalcost/10;
        ObjFun_Mass_term(1,loop)=Mass_weight*TotalMass;
        ObjFun_Penalty_term(1,loop)=Penalty;
        error_messages;
    end
end

if n_variable==4
    hm_max=15;
    hm_min=8;
    tw_max=18;
    tw_min=10;
    for loop=1:no_loops
        PSOnew_4_1_18_four_vars_OptiAFPM;
        la_out(1,loop)=Blue;
        wm_out(1,loop)=Red;
        hm_out(1,loop)=Green;
        tw_out(1,loop)=Yellow;
        Particle_Output_out(1,loop)=Particle_Output;
        Total_Mass_out(1,loop)=TotalMass;
        Total_Cost_out(1,loop)=Totalcost;
        Efficiency_Min_out(1,loop)=efficiency_real;
        ObjFun_Eff_term(1,loop)=Eff_weigth*(1-efficiency_real)*200;
        ObjFun_Cost_term(1,loop)=Cost_weight*Totalcost/10;
        ObjFun_Mass_term(1,loop)=Mass_weight*TotalMass;
        ObjFun_Penalty_term(1,loop)=Penalty;
        error_messages;
    end
end

% cleanup variables
display 'Removing unused variables'
for i = 1 : rows(unused_vars)
  if eval(sprintf('exist %s\n', unused_vars(i,:))) == 1
    eval(sprintf('clear %s\n', unused_vars(i,:)));
  endif
endfor
%save variables
display 'Storing variables'
eval(sprintf('save -mat7-binary %s/variables.mat',sim_output_path));

%generate JSON data
status = 'success';
status_message = '';

display 'Generating JSON output'
JSON_Output;
%json = JSON_Output('success', '', ai, angledeg,Bmgfile,Bmg_real,Bmaxfile,cq,dc,efficiency_real,EMF_cut_in,Iac_nom,Jmax,kd,la,mcu_constr,n_cut_in,n_nom,Nc,Optimal_airgap,Phl,Pnom_dc,ResHP_70,Rin,Rout_eff,Totalcost,wc,wm);

filename = [sim_output_path,'/result.json'];
fid = fopen (filename, 'w');
fputs (fid, json);
fclose (fid);

display 'Simulation complete'
