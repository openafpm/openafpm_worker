import threading
import time
import requests
import windsim
import config
import os
import socket

exitFlag = 0

class poller (threading.Thread):
   def __init__(self, threadID, name):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.daemon = True
   def run(self):
      print "Starting worker on " + self.name + "\n"
      get_one(self.name, 10, self.name)
      print "Exiting worker on " + self.name + "\n"

def get_one(threadName, delay, workerID):
   headers = {'api-key':'veIYKf1OQuKD8vQhf7YbRA'}
   while 1:
      if exitFlag:
         threadName.exit()
      try:
        r = requests.post(config.endpoint_url + '/rest/rules/rules_get_one',
          data = {'api-key':'veIYKf1OQuKD8vQhf7YbRA','worker_id': workerID},
          headers = headers)
        result = r.json()
        # print result;
        if result['nid'] is None:
           # print "No sim pending"
           time.sleep(delay)
           continue
      except Exception as e:
        print e
        time.sleep(delay)
        continue

      # Notify sim start
      try:
        packaged_data = {
           "field_simulation_status":{
             "und" :{
               "value": 3
             }
           }
         }
        r = requests.put(config.endpoint_url + '/rest/node/'+result['nid'],
         json=packaged_data, headers = headers)
        # start simulation
        result_data, messages = windsim.start_simulation(result['nid'], result['simulation_type'])
        packaged_data = {
         "field_json_result": {
           "und": {
             "0": {
               "value":result_data
             }
           }
         },
         "field_simulation_status":{
           "und" :{
             "value": 0
           }
         },
         "field_simulation_messages":{
           "und" :{
              "0": {
                "value":messages
              }
           }
         }
        }
        # upload file
        url=config.endpoint_url + '/rest/node/'+result['nid']+'/attach_file'
        params = {
          'attach':'1',
          'field_name':'field_variable_file'
        }
        files = {
          'files[files]': (
            'simulation-'+result['nid']+'.mat',
            open(config.workspace_path+'simulation-'+result['nid']+'/variables.mat', 'rb'),
            'application/octet-stream'
            )
          }

        r = requests.post(url, headers=headers, data=params, files=files)
        # Upload FEMM image
        if os.path.isfile(config.workspace_path+'simulation-'+result['nid']+'/output/FEMM_screenshot.png'):
            params = {
              'attach':'1',
              'field_name':'field_femm_image'
            }
            files = {
              'files[files]': (
                'FEMM_screenshot.png',
                open(config.workspace_path+'simulation-'+result['nid']+'/output/FEMM_screenshot.png', 'rb'),
                'application/octet-stream'
                )
              }

            r = requests.post(url, headers=headers, data=params, files=files)
        # print r.json()

      except Exception as e:
         print e
         print "simulation failed"

         packaged_data = {
           "field_simulation_status":{
             "und" :{
               "value": 4
             }
           }
         }
      # Send result

      r = requests.put(config.endpoint_url + '/rest/node/'+result['nid'],
       json=packaged_data, headers = headers)
      # print r.json()

def start():
   # Create new threads
   workerID = socket.gethostname();
   poller_thread = poller(1, workerID)

   # Start new Threads
   poller_thread.start()
