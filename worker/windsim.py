import requests
import config
import os
import subprocess
import shutil
from PIL import Image
##
#Retrieve simulation data
#
def getSimData(sim_id):
    # TODO: make this conditional for Opti only
  sim_data=''
  try:
    payload = {'api-key':'veIYKf1OQuKD8vQhf7YbRA'}
    headers = {'api-key':'veIYKf1OQuKD8vQhf7YbRA'}
    r = requests.get(config.endpoint_url + '/rest/sim-data?args[0]='+str(sim_id),
     headers = headers)
    # print r.url
    # print r.json()
    for item in r.json():
      for var in item:
          if len(item[var]) > 0:
            try:
              sim_data += var+'='+str(int(item[var]))+';\n'
            except:
              try:
                sim_data += var+'='+str(float(item[var]))+';\n'
              except:
                sim_data += var+'=\''+item[var]+'\';\n'
  except Exception as e:
    print e
  return sim_data

##
# execute simulation
#

def start_simulation(sim_id, tool):
  tool_folder = {
    'MagnAFPM':'MagnAFPM',
    'UserAFPM':'UserAFPM',
    'OptiAFPM':'OptiAFPM'
  }
  sim_workspace = config.workspace_path+'simulation-'+str(sim_id)
  tool_path = config.openafpm_path + "/" + tool_folder.get(tool, '')

  # Setup workspace
  try:
    if os.path.exists(sim_workspace):
        print "Removing existing workspace"
        shutil.rmtree(sim_workspace)
    print "Creating " + sim_workspace
    os.mkdir(sim_workspace)
    os.mkdir(sim_workspace + '/output')
  except Exception as e:
    print e
    print "Error creating workspace"
    return {}

  print "Initializing "+tool+" Simulation No: "+str(sim_id)
  # Generate startup file
  fh_simstart = file(sim_workspace+'/simstart.m', 'w')
  fh_simstart.write("disp 'Starting simulation "+str(sim_id)+"'\n"+
  "clear all\n"+
  "addpath('"+tool_path+"');\n"+
  "addpath('"+sim_workspace+"');\n"+
  getSimData(sim_id) +
  "global location_path;\n" +
  "sim_output_path = '"+sim_workspace+"/' ;\n"+
  "location_path='"+sim_workspace+"/output/';\n"+
  "source "+tool_path+"/calc_gen.m;\n")
  fh_simstart.close()

  # Generate input variables file
  fh_input_vars = file(sim_workspace+'/input_vars.m', 'w')
  fh_input_vars.write(getSimData(sim_id))
  fh_input_vars.close()

  # Generate execution script
  fh_es = file(sim_workspace+'/start.sh', 'w')
  fh_es.write("#!/bin/bash\n"+
  "export DISPLAY=:3.0\n"+
  # "export WINEDEBUG=-all\n"+
  "cd "+sim_workspace+"/output\n"+
  "octave -W "+sim_workspace+"/simstart.m\n"+
  "wineserver -k\n")
  fh_es.close()
  print "Starting simulation:" +str(sim_id)
  process = subprocess.Popen(['bash', sim_workspace+'/start.sh'], stdout=subprocess.PIPE)
  messages=''
  while True:
    output = process.stdout.readline()
    if output == '' and process.poll() is not None:
        break
    if output:
        if output.startswith('Warning:') or output.startswith('Construction limitation:'):
            messages += output
        print output.strip()

  with open(sim_workspace+'/result.json', 'r') as result_file:
   result_data = result_file.read()

  try:
    img = Image.open(sim_workspace+"/output/FEMM_screenshot.bmp")
    img.save(sim_workspace+"/output/FEMM_screenshot.png")
  except Exception as e:
    print "No FEMM image\n"

  return result_data, messages
