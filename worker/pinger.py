import threading
import time
import requests
import config

exitFlag = 0

class pinger (threading.Thread):
   def __init__(self, threadID, name):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.daemon = True
   def run(self):
      print "Starting " + self.name + "\n"
      ping(self.name, 10)
      print "Exiting " + self.name + "\n"

def ping(threadName, delay):
  time.sleep(delay/2)
  while 1:
    if exitFlag:
       threadName.exit()
    try:
      headers = {'api-key':'veIYKf1OQuKD8vQhf7YbRA'}
      r = requests.post(config.endpoint_url + 'rest/rules/rules_ping',
       data = {'worker_id': 'home'}, headers = headers)
      # print "Ping"
    except Exception as e:
      print e
    time.sleep(delay)
def start():
   # Create new threads
   pinger_thread = pinger(2, "Pinger")

   # Start new Threads
   pinger_thread.start()
