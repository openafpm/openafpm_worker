---
- hosts: worker
  become: true
  gather_facts: False
  pre_tasks:
  - raw: test -e /usr/bin/python || (apt -y update && apt install -y python)
  - setup:
  tasks:
  - name: install xvfb
    apt: name=xvfb state=present update_cache=yes
  - name: install x11vnc
    apt: name=x11vnc state=present update_cache=yes
  - name: install python-requests
    apt: name=python-requests state=present update_cache=yes
  - name: install octave
    apt: name=octave state=present update_cache=yes
  - name: install python-pil
    apt: name=python-pil state=present update_cache=yes
  - name: install python-pip
    apt: name=python-pip state=present update_cache=yes
  - name: install sentry sdk
    pip:
      name:
        - urllib3==1.22
        - sentry-sdk==0.7.9

  # worker
  - name: Clone OpenAFPM worker
    git:
      repo: https://gitlab.com/openafpm/openafpm_worker.git
      dest: /opt/OpenAFPM
  - name: Add OpenAFPM service user
    user:
      name: 'openafpm'
      home: '/opt/OpenAFPM'
      # shell: '/bin/false'
      state: 'present'
    become: True
  - name : Set ownership
    file:
      path: /opt/OpenAFPM
      owner: openafpm
      group: openafpm
      # recurse: yes

  # XFEMM setup
  - name: install wine
    apt: name=wine-stable state=present update_cache=yes
  - name: install wine32
    apt: name=wine32 state=present update_cache=yes
  - name: Download FEMM42
    get_url:
      url: http://www.femm.info/wiki/Files/files.xml?action=download&file=femm42bin_x64_12Jan2016.exe
      dest: /opt/OpenAFPM/femm42.exe
  - name: install femm setup script
    template:
      src: templates/femm_setup.sh.j2
      dest: /opt/OpenAFPM/femm_setup.sh
      mode: 0755
  - debug:
      msg: "Please use a VNC client to connect to the following address: {{hostvars[inventory_hostname]['ansible_default_ipv4']['address']}}"
  - name: Run wine femm setup
    command: /opt/OpenAFPM/femm_setup.sh
    args:
      creates: /opt/OpenAFPM/.wine/drive_c/femm42
    become: True
  - name: Set root path in openfemm.m file
    replace:
      path: /opt/OpenAFPM/.wine/drive_c/femm42/mfiles/openfemm.m
      regexp: '(\s+)rootdir=''.*'';'
      replace: "\nif (exist (\"OCTAVE_VERSION\", \"builtin\") > 0)
          \nrootdir=tilde_expand('~/.wine/drive_c/femm42/bin/');
        \nelse
          \nrootdir=('~/.wine/drive_c/femm42/bin/');
        \nend\n"
  - name: Set system call in openfemm.m file
    replace:
      path: /opt/OpenAFPM/.wine/drive_c/femm42/mfiles/openfemm.m
      regexp: '(\s+)system\(\[rootdir.*'
      replace: "\nsystem(['wine \"',rootdir,'femm.exe\" -filelink'],0,'async');\n"
  - name: Replace sleep with pause in openfemm.m file
    replace:
      path: /opt/OpenAFPM/.wine/drive_c/femm42/mfiles/openfemm.m
      regexp: 'sleep\('
      replace: "pause("
  - name: Replace sleep with pause in callfemm.m file
    replace:
      path: /opt/OpenAFPM/.wine/drive_c/femm42/mfiles/callfemm.m
      regexp: 'sleep\('
      replace: "pause("
  - name: Replace sleep with pause in callfemm_noeval.m file
    replace:
      path: /opt/OpenAFPM/.wine/drive_c/femm42/mfiles/callfemm_noeval.m
      regexp: 'sleep\('
      replace: "pause("

#  Service
  - name: install worker service
    template:
      src: templates/OpenAFPM.service.j2
      dest: /etc/systemd/system/OpenAFPM.service
      mode: 0644
    become: true
  - name: Start worker service, if not started
    systemd:
      daemon_reload: True
      name: 'OpenAFPM'
      enabled: True
      state: 'started'
    become: True
