# OpenAFPM worker

This is a worker program for wind turbine simulation

Simulation is carried out by AFPM octave scripts and [FEMM42](http://www.femm.info/wiki/HomePage)

## Contents
*AFPM: Octave simulation tools  
worker: Service for fetching and running simulations from web  

# Installation

## Local setup

Clone repo  
```git clone https://gitlab.com/drid/NTUA_wind_worker.git /opt/OpenAFPM  ```
```cd /opt/OpenAFPM```
  

Run Debian/Ubuntu install script *install.sh*  
```./install.sh  ```

Install FEMM on wine for the user that will run the worker  

## Ansible setup

Clone repo  
```git clone https://gitlab.com/drid/NTUA_wind_worker.git```

```cd ansible  ```

setup your hosts  

```ansible-playbook workers.yaml  ```

# Architecture

Each worker polls the web service for pending simulation requests.  
A set of octave scripts runs the simulation and generates results  
Worker uploads results to the web service  

Note:
FEMM42 runs under wine and it uses Xvfb as it needs a display  

# License

GNU General Public License v3.0
