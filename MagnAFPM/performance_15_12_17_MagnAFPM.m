clear n freqN Ef deltaN deltaNdeg VtN PelN PcopperN PeddyN PHMN ProtN PmechmeanN Torque_x_avg THMN TmechmeanN Trot effHMN efftotalN Vdc Idc Pdc

%Fluxoc=FluxRms(1); % do not take into account chages in flux linkage due to currents
Fluxoc=FluxRms; % do take into account chages in flux linkage due to currents

%Bp=Bpmax(1); % do not take into account chages in Bp due to currents
Bp=Bpmax; % do take into account chages in Bp due to currents

N_max=n_nom; % maximum rpm to run
N_min=0; % minimum rpm to run
N_step=n_nom/RPM_step; % step to increase rpm by - User defined with values 1,5,10 and 20

%I_step=1; % when you do not take into account changes in flux linkage
%Imax=12; % when you do not take into account changes in flux linkage

Irms=0:I_step:Imax; % current range and step as in solver

for i=1:(Imax/I_step+1)
    n(1)=N_min; % start from minimum rpm
    for j=1:(N_max/N_step+1)
        freqN(j)=n(j)*poles/120; % calculate frequency
        Fluxoc=FluxRms(i); % in order to change flux linkage with current
        Bp=Bpmax(i); % in order to change Bp with current
        Ef(i,j)=q*Fluxoc*(poles/2)*2*pi*n(j)/60; % calculate EMF from 
        deltaN(i)=asin(Irms(i)*2*pi*freqN(j)*Ls/Ef(i,j)); % power angle in radians
        deltaNdeg(i)=deltaN(i)*180/pi; % power angle in degrees
        if Ef(i,j)*cos(deltaN(i))>=Irms(i)*q*Rc % if currents flow then calulate Vt 
            VtN(i,j)=Ef(i,j)*cos(deltaN(i))-Irms(i)*q*Rc; % calculate terminal voltage
        else
            VtN(i,j)=0; % no current - no load - open circuit voltage
        end
        PelN(i,j)=3*VtN(i,j)*Irms(i); % electrical power
        PcopperN(i)=3*Irms(i)^2*q*Rc; % copper losses
        PeddyN(j)=(pi*la*10^(-3)*((dc_theor*10^(-3))^4)*(Bp^2)*((2*pi*n(j)*poles/120)^2)*nphase*Nc)/(32*pt); % eddy current losses in coils
        PHMN(i,j)=PelN(i,j)+PcopperN(i)+PeddyN(j); % electromagnetic power
        ProtN(j)=0.06*2*(BackIronMass+blade_rotor_mass+MagnetMass*10^(-3)+BearingHubMass*10^(-3))*n(j)/60; % frictional losses in the bearing (div by rev/s)
        PmechmeanN(i,j)=PHMN(i,j)+ProtN(j); % mechanical power
        THMN(i,j)=PHMN(i,j)*60/(2*pi*n(j)); % electromagnetic torque
        TmechmeanN(i,j)=PmechmeanN(i,j)*60/(2*pi*n(j)); % mechanical torque
        Trot(j)=ProtN(j)*60/(2*pi*n(j)); % Torque of friction losses
        effHMN(i,j)=PelN(i,j)/PHMN(i,j)*100; % electromagnetic efficiency
        efftotalN(i,j)=(PelN(i,j)/PmechmeanN(i,j))*100; % generator efficiency
        efftotalN(1,j)=0; % zero efficiency for zero rpm
        if deltaN(i)==0
            Vdc(i,j)=VtN(i,j)*sqrt(3)*1.35; % DC voltage after rectifier for Open circuit voltages
        else
            Vdc(i,j)=VtN(i,j)*sqrt(3)*1.35*((100-trans_loss)/100); % DC voltage after transmission losses on cable + rectifier losses
        end
        Idc(i)=sqrt(3/2)*Irms(i); % DC current after rectifier
        Pdc(i,j)=Vdc(i,j)*Idc(i); % DC power atfer rectifier
        if j~=(N_max/N_step+1) % stop at final value of rpm or add rpm step
            n(j+1)=n(j)+N_step;
        end
    end
end

% calculate electromagnetic torque from FEMM

%get SIZE
if step>3
    tempvar=size(fluxlinkageB);
    SIZE=tempvar(1,2);
end 

Torque_x_avg(i,1)=0;
for xx=1:i
    for yy=1:SIZE
    Torque_x_avg(xx)=Torque_x(xx,yy)+Torque_x_avg(xx);
    end
Torque_x_avg(xx)=-(Torque_x_avg(xx)/SIZE);
end

% calculate power from measured from FEMM electromagnetic torque for Iac_nom and n_nom
PHMN_Torque_x=Torque_x_avg(Iac_nom/I_step+1)*(2*pi*n_nom/60); % Electromagnetic power for Imax and n_nom
PcopperN_Torque_x=3*Iac_nom^2*q*Rc; % copper losses for Imax
PeddyN_Torque_x=(pi*la*10^(-3)*((dc_theor*10^(-3))^4)*(Bp^2)*((2*pi*n_nom*poles/120)^2)*nphase*Nc)/(32*pt); % eddy current losses in coils for Imax and n_nom
PelN_Torque_x=PHMN_Torque_x-PcopperN_Torque_x-PeddyN_Torque_x; % Electrical Power for Imax and n_nom

% rotational losses for Iac_nom and n_nom
ProtN_Torque_x=0.06*2*(BackIronMass+blade_rotor_mass+MagnetMass*10^(-3)+BearingHubMass*10^(-3))*n_nom/60; % frictional losses in the bearing (div by rev/s)
Pmech_Torque_x=PHMN_Torque_x+ProtN_Torque_x; % mechanical power of losses

% efficiecncy for Iac_nom and n_nom
efftotalN_Torque_x=(PelN_Torque_x/Pmech_Torque_x); % generator efficiency

% Power to batteries for Iac_nom and n_nom
PelN_Torque_x_rect=PelN_Torque_x*((100-trans_loss)/100); % Losses on cables at Vw nominal
PelN_Torque_x_dc=PelN_Torque_x_rect*((100-rect_loss)/100); % Losses on the rectifier
