if rotor == 1 || rotor ==2 % move top magnets to the other side

mi_selectsegment(L_half+ypol-0.001,hr+g+tw+g+hm);% change group in extra part
mi_setsegmentprop('',msize,0,0,8);
mi_selectsegment(L_half+ypol-0.001,hr+g+tw+g);
mi_setsegmentprop('',msize,0,0,8);
mi_selectsegment(L_half+ypol,hr+g+tw+g+0.1);
mi_setsegmentprop('',msize,0,0,8);
mi_selectgroup(8);
mi_copytranslate(-L_half,0,1); % copy extra part to the other side
mi_clearselected
mi_selectsegment(ypol,hr+g+tw+g+hm-0.1); % change back to group 3 with rest of magnets
mi_setsegmentprop('',msize,0,0,3);
mi_clearselected

mi_selectsegment(L_half-0.1,hr+g+tw+g); % change group for top and bottom segments
mi_setsegmentprop('',msize,0,0,11);
mi_selectsegment(L_half-0.1,hr+g+tw+g+hm);
mi_setsegmentprop('',msize,0,0,11);
mi_clearselected

mi_selectsegment(L_half+ypol-0.001,hr+g+tw+g+hm); % delete extra part segments and nodes
mi_selectsegment(L_half+ypol-0.001,hr+g+tw+g);
mi_selectsegment(L_half+ypol,hr+g+tw+g+0.1);
mi_deleteselected
mi_selectnode(L_half+ypol,hr+g+tw+g+hm);
mi_selectnode(L_half+ypol,hr+g+tw+g);
mi_deleteselected

mi_selectsegment(L_half,hr+g+tw+g+hm-0.1); % set boundaries
mi_setsegmentprop('Periodic4',msize,0,0,1);
mi_selectsegment(0,hr+g+tw+g+hm-0.1);
mi_setsegmentprop('Periodic4',msize,0,0,1);
mi_clearselected

mi_selectlabel(L_half-wm/2+ypol,hr+g+tw+g+hm/2);% set block label for half magnet
temp_var=mi_selectlabel(L_half-wm/2+ypol,hr+g+tw+g+hm/2); % get exact position of label
mi_selectlabel(L_half-wm/2+ypol,hr+g+tw+g+hm/2);
mi_copytranslate(0.001-temp_var(1),0,1);
mi_copytranslate(0.001-(L_half-wm/2+ypol),0,1);
mi_clearselected
mi_selectlabel(0.001,hr+g+tw+g+hm/2);
mi_setblockprop(mag_mater,0,msize,'',mag_dir,10,0);
mi_clearselected

mi_selectlabel(L_half-wm/2+ypol,hr+g+tw+g+hm/2); % set block label for other half magnet
temp_var=mi_selectlabel(L_half-wm/2+ypol,hr+g+tw+g+hm/2); % get exact position of label
mi_selectlabel(L_half-wm/2+ypol,hr+g+tw+g+hm/2);
mi_setblockprop(mag_mater,0,msize,'',mag_dir,10,0);
mi_movetranslate(L_half-temp_var(1)-0.001,0)
mi_clearselected

end

if rotor == 2 % move bottom magnets to the other side
    
mi_selectsegment(L_half+ypol-0.001,hr); % change group in extra part
mi_setsegmentprop('',msize,0,0,15);
mi_selectsegment(L_half+ypol-0.001,hr-hm);
mi_setsegmentprop('',msize,0,0,15);
mi_selectsegment(L_half+ypol,hr-hm+0.1);
mi_setsegmentprop('',msize,0,0,15);
mi_selectgroup(15);
mi_copytranslate(-L_half,0,1); % copy extra part to the other side
mi_clearselected
mi_selectsegment(ypol,hr-0.1); % change back to group 3 with rest of magnets
mi_setsegmentprop('',msize,0,0,3);
mi_clearselected

mi_selectsegment(L_half-0.1,hr); % change group for top and bottom segments
mi_setsegmentprop('',msize,0,0,14);
mi_selectsegment(L_half-0.1,hr-hm);
mi_setsegmentprop('',msize,0,0,14);
mi_clearselected

mi_selectsegment(L_half+ypol-0.001,hr); % delete extra part segments and nodes
mi_selectsegment(L_half+ypol-0.001,hr-hm);
mi_selectsegment(L_half+ypol,hr+0.1);
mi_deleteselected
mi_selectnode(L_half+ypol,hr);
mi_selectnode(L_half+ypol,hr-hm);
mi_deleteselected

mi_selectsegment(L_half,hr-0.1); % set boundaries for magnet
mi_setsegmentprop('Periodic5',msize,0,0,1);
mi_selectsegment(0,hr-0.1);
mi_setsegmentprop('Periodic5',msize,0,0,1);
mi_clearselected

mi_addsegment(L_half,hr,L_half,hr+g+tw+g)
mi_selectsegment(L_half,hr+0.1); % set boundaries for air
mi_setsegmentprop('Periodic1',msize,0,0,1);
mi_selectsegment(0,hr+0.1);
mi_setsegmentprop('Periodic1',msize,0,0,1);
mi_clearselected

mi_selectlabel(L_half-wm/2+ypol,hr-hm/2); % set block label for half magnet
temp_var=mi_selectlabel(L_half-wm/2+ypol,hr-hm/2); % get exact position of label
mi_selectlabel(L_half-wm/2+ypol,hr-hm/2);
mi_copytranslate(0.001-temp_var(1),0,1);
mi_copytranslate(0.001-(L_half-wm/2+ypol),0,1);
mi_clearselected
mi_selectlabel(0.001,hr-hm/2);
mi_setblockprop(mag_mater,0,msize,'',mag_dir,10,0);
mi_clearselected

mi_selectlabel(L_half-wm/2+ypol,hr-hm/2); % set block label for other half magnet
temp_var=mi_selectlabel(L_half-wm/2+ypol,hr-hm/2); % get exact position of label
mi_selectlabel(L_half-wm/2+ypol,hr-hm/2);
mi_setblockprop(mag_mater,0,msize,'',mag_dir,10,0);
mi_movetranslate(L_half-temp_var(1)-0.001,0)
mi_clearselected

end

if rotor == 0 % move top magnets to the other side
    
mi_selectsegment(L_half+ypol-0.001,extra+hr+g+tw+g+hm);% change group in extra part
mi_setsegmentprop('',msize,0,0,8);
mi_selectsegment(L_half+ypol-0.001,extra+hr+g+tw+g);
mi_setsegmentprop('',msize,0,0,8);
mi_selectsegment(L_half+ypol,extra+hr+g+tw+g+0.1);
mi_setsegmentprop('',msize,0,0,8);
mi_selectgroup(8);
mi_copytranslate(-L_half,0,1); % copy extra part to the other side
mi_clearselected
mi_selectsegment(ypol,extra+hr+g+tw+g+hm-0.1); % change back to group 3 with rest of magnets
mi_setsegmentprop('',msize,0,0,3);
mi_clearselected

mi_selectsegment(L_half-0.1,extra+hr+g+tw+g); % change group for top and bottom segments
mi_setsegmentprop('',msize,0,0,11);
mi_selectsegment(L_half-0.1,extra+hr+g+tw+g+hm);
mi_setsegmentprop('',msize,0,0,11);
mi_clearselected

mi_selectsegment(L_half+ypol-0.001,extra+hr+g+tw+g+hm); % delete extra part segments and nodes
mi_selectsegment(L_half+ypol-0.001,extra+hr+g+tw+g);
mi_selectsegment(L_half+ypol,extra+hr+g+tw+g+0.1);
mi_deleteselected
mi_selectnode(L_half+ypol,extra+hr+g+tw+g+hm);
mi_selectnode(L_half+ypol,extra+hr+g+tw+g);
mi_deleteselected

mi_selectsegment(L_half,extra+hr+g+tw+g+hm-0.1); % set boundaries
mi_setsegmentprop('Periodic4',msize,0,0,1);
mi_selectsegment(0,extra+hr+g+tw+g+hm-0.1);
mi_setsegmentprop('Periodic4',msize,0,0,1);
mi_clearselected

mi_selectlabel(L_half-wm/2+ypol,extra+hr+g+tw+g+hm/2);% set block label for half magnet
temp_var=mi_selectlabel(L_half-wm/2+ypol,extra+hr+g+tw+g+hm/2); % get exact position of label
mi_selectlabel(L_half-wm/2+ypol,extra+hr+g+tw+g+hm/2);
mi_copytranslate(0.001-temp_var(1),0,1);
mi_copytranslate(0.001-(L_half-wm/2+ypol),0,1);
mi_clearselected
mi_selectlabel(0.001,extra+hr+g+tw+g+hm/2);
mi_setblockprop(mag_mater,0,msize,'',mag_dir,10,0);
mi_clearselected

mi_selectlabel(L_half-wm/2+ypol,extra+hr+g+tw+g+hm/2); % set block label for other half magnet
temp_var=mi_selectlabel(L_half-wm/2+ypol,extra+hr+g+tw+g+hm/2); % get exact position of label
mi_selectlabel(L_half-wm/2+ypol,extra+hr+g+tw+g+hm/2);
mi_setblockprop(mag_mater,0,msize,'',mag_dir,10,0);
mi_movetranslate(L_half-temp_var(1)-0.001,0)
mi_clearselected

end
