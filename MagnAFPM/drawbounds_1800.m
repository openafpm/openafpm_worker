if rotor == 1 

% draw boundary line segments and give them periodic conditions in pairs

mi_drawline(0,hr,0,hr+g+tw+g+hm);
mi_selectsegment(0,hr+1);
mi_setsegmentprop('Periodic1',msize,0,0,1);
mi_clearselected

mi_drawline(L_half,hr,L_half,hr+g+tw+g+hm);
mi_selectsegment(L_half,hr+1);
mi_setsegmentprop('Periodic1',msize,0,0,1);
mi_clearselected

mi_selectsegment(0,1);
mi_setsegmentprop('Periodic2',msize,0,0,4);
mi_clearselected
mi_selectsegment(L_half,1);
mi_setsegmentprop('Periodic2',msize,0,0,4);
mi_clearselected

mi_selectsegment(0,hr+g+tw+g+hm+1);
mi_setsegmentprop('Periodic3',msize,0,0,4);
mi_clearselected
mi_selectsegment(L_half,hr+g+tw+g+hm+1);
mi_setsegmentprop('Periodic3',msize,0,0,4);
mi_clearselected

% give boundary condition for top and bottom of rotor disks

mi_selectsegment(L_half/2,0);
mi_setsegmentprop('A=0',msize,0,0,4);
mi_clearselected

mi_selectsegment(L_half/2,hr+g+tw+g+hm+hr);
mi_setsegmentprop('A=0',msize,0,0,4);
mi_clearselected

elseif rotor == 2 || rotor == 0
    
% draw boundary line segments and give them periodic conditions in pairs

mi_drawline(0,(hr-hm),0,hr+g+tw+g+hm);
mi_selectsegment(0,(hr-hm)+1);
mi_setsegmentprop('Periodic1',msize,0,0,1);
mi_clearselected

mi_drawline(L_half,(hr-hm),L_half,hr+g+tw+g+hm);
mi_selectsegment(L_half,(hr-hm)+1);
mi_setsegmentprop('Periodic1',msize,0,0,1);
mi_clearselected

mi_selectsegment(0,1);
mi_setsegmentprop('Periodic2',msize,0,0,4);
mi_clearselected
mi_selectsegment(L_half,1);
mi_setsegmentprop('Periodic2',msize,0,0,4);
mi_clearselected

mi_selectsegment(0,hr+g+tw+g+hm+1);
mi_setsegmentprop('Periodic3',msize,0,0,4);
mi_clearselected
mi_selectsegment(L_half,hr+g+tw+g+hm+1);
mi_setsegmentprop('Periodic3',msize,0,0,4);
mi_clearselected

% give boundary condition for top and bottom of rotor disks

mi_selectsegment(L_half/2,0);
mi_setsegmentprop('A=0',msize,0,0,4);
mi_clearselected

mi_selectsegment(L_half/2,hr+g+tw+g+hm+(hr-hm));
mi_setsegmentprop('A=0',msize,0,0,4);
mi_clearselected

end
