% calculates the maximum value of |B| of the back iron disk at hr/2

if rotor == 2
% draw line at the middle of the disk
mo_addcontour(0,hr+g+tw/2+tw/2+g+hr-hr_half);
mo_addcontour(L_half,hr+g+tw/2+tw/2+g+hr-hr_half);

elseif rotor == 1
% draw line at the middle of the disk
mo_addcontour(0,hr+g+tw/2+tw/2+g+hm+hr/2);
mo_addcontour(L_half,hr+g+tw/2+tw/2+g+hm+hr/2);

elseif rotor == 0
% draw line at the middle of the disk
mo_addcontour(0,extra+hr+g+tw/2+tw/2+g+hr-hr_half);
mo_addcontour(L_half,extra+hr+g+tw/2+tw/2+g+hr-hr_half);

end

path=[location_path,'Saturfile.txt'];
mo_makeplot(1,150,path,1); % make plot of Bn with 150 points and save file
%mo_makeplot(1,150); % make plot of Bn with 150 points and save file

Saturfile = load(path); % load data
L_Satur = Saturfile(:,1); % load first column in x
B_Satur = Saturfile(:,2); % load second column in SSS

SaturMAX=B_Satur(1); % find largest magnitude of B
for i=1:150
   if B_Satur(i)>=SaturMAX
        SaturMAX=B_Satur(i);
    end
end

mo_clearcontour(); % clear contour for a complete period of Bn normal flux density
mo_clearcontour();
