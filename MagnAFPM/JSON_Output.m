clear json;
output_var_names = who;
json="";

function jsonstr = qstr2json(varname, varval)
  jsonstr =  sprintf ('\t"%s": "%s",\n', varname, varval);
endfunction

function jsonsc = scalar2json(varname, varval)
  jsonsc =  sprintf ('\t"%s": %d,\n', varname, varval);
endfunction

function jsonm = mat2json(varname, m)
  jsonm = sprintf ('\t"%s": [', varname );
  for row=1:rows(m)
    jsonm =  strcat(jsonm, "[");
    jsonm =  strcat(jsonm,  sprintf ('%f,', m(row,1:end-1)));
    jsonm =  strcat(jsonm,  sprintf ('%f', m(row,end)));
    if row == rows(m)
      jsonm =  strcat(jsonm, sprintf ("]"));
    else
      jsonm =  strcat(jsonm, sprintf ("],"));
    end
  end
  jsonm = strcat (jsonm, sprintf ('],\n'));
  jsonm = strrep(jsonm, ",-Inf", ",\"-Inf\"");
  jsonm = strrep(jsonm, ",Inf", ",\"Inf\"");
  jsonm = strrep(jsonm, "[-Inf", "[\"-Inf\"");
  jsonm = strrep(jsonm, "[Inf", "[\"Inf\"");
  jsonm = strrep(jsonm, "NaN", "null");
  
endfunction

json = strcat (json, sprintf('{\n'));
for i = 1 : rows(output_var_names)

  varname = output_var_names{i,:};
  if strcmp(varname, 'ans')
    continue
  endif
  if strcmp(varname, 'sim_output_path')
    continue
  endif
  varval = eval(sprintf ('%s\n', varname));
  vartype = typeinfo(varval);
%  printf ('%s %s\n', varname, vartype);
  
  if strcmp(vartype, 'matrix') || strcmp(vartype, 'range')
    json = strcat (json, mat2json(varname, varval));
  elseif strcmp(vartype, 'scalar')
    json = strcat (json, scalar2json(varname, varval));
  elseif strcmp(vartype, 'sq_string')
    json = strcat (json, qstr2json(varname, varval));
  endif
  
endfor
json = strcat (json(1:end-2), sprintf( '\n}'));