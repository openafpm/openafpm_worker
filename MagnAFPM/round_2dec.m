% function round_2dec rounds to the fourth decimal point

function [result]= round_2dec(x)
answer=x*10000;
result=round(answer)/10000;

