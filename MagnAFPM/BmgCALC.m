% calculates the magnitude of the first harmonic of the normal flux density

if rotor == 1 || rotor == 2
% reduce 5% from each side of magnet to avoid spikes
mo_addcontour(dist_magnet/2+wm*0.05,hr+g+tw+g); % draw contour for a magnet surface
mo_addcontour(dist_magnet/2+wm-wm*0.05,hr+g+tw+g);

elseif rotor == 0

mo_addcontour(dist_magnet/2+wm*0.05,extra+hr+g+tw+g); % draw contour for a magnet surface
mo_addcontour(dist_magnet/2+wm-wm*0.05,extra+hr+g+tw+g);
%mo_addcontour(L_half,extra+hr+g+tw/2); % draw contour for two periods

end

path=[location_path,'Bmgfile.txt'];
mo_makeplot(2,150,path,1); % make plot of Bn with 150 points and save file
%mo_makeplot(2,150); % make plot of Bn with 150 points and save file

Bmgfile = load(path); % load data
y = Bmgfile(:,1); % load firts column in y
BMG = Bmgfile(:,2); % load second column in BMG

mo_clearcontour(); % clear contour for a complete period of Bn normal flux density
mo_clearcontour();

SIZE=size(BMG); % find average value for Bmg
addition=0;
for i=1:(SIZE(1,1))
    addition=BMG(i,1)+addition;
end
Bmg=addition/SIZE(1,1);
%Bmg=min(BMG) %find minimum value
