#!/bin/bash
echo installing packages
dpkg --add-architecture i386
apt update
apt install xvfb x11vnc wine python python-requests octave python-pil
pip install urllib3==1.22
pip install --upgrade sentry-sdk==0.7.9

echo creating user
useradd openafpm -d /opt/OpenAFPM

echo installing service
cp OpenAFPM.service /etc/systemd/system/
systemctl enable OpenAFPM.service
