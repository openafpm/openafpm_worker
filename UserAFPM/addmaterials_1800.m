% Add materials to library
mi_addmaterial('Pure Iron',14872,14872,0,0,10.44,0,0,1,0,0,0,0,0);
mi_addmaterial('Air',1,1,0,0,0,0,0,0,0,0,0,0,0);
mi_addmaterial('NdFeB N35',1.05,1.05,(155319*sqrt(35)),0,0.667,0,0,0,0,0,0,0,0);
mi_addmaterial('NdFeB N40',1.05,1.05,(155319*sqrt(40)),0,0.667,0,0,0,0,0,0,0,0);
mi_addmaterial('NdFeB N42',1.05,1.05,(155319*sqrt(42)),0,0.667,0,0,0,0,0,0,0,0);
mi_addmaterial('NdFeB N45',1.05,1.05,(155319*sqrt(45)),0,0.667,0,0,0,0,0,0,0,0); % aferontas 0.3% apo to (155319*sqrt(45))
mi_addmaterial('NdFeB N52',1.05,1.05,(155319*sqrt(52)),0,0.667,0,0,0,0,0,0,0,0);
mi_addmaterial('Ceramic 8',1.43846,1.43846,227950,0,0,0,0,0,0,0,0,0,0);
%mi_getmaterial('Ceramic 8'); % me auto egine h diplomatikh ths Katerinas!
% add copper conductor of diameter dc
mi_addmaterial('dc mm',1,1,0,0,58,0,0,1,0,0,0,1,dc);

% From: http://www.femm.info/wiki/PermanentMagnetExample
% FEMM has a selection of built-in NdFeB materials, but the materials library is not comprehensive. If you want to model a class that is not in the library, you can build you own magnet model in the materials library. A pretty good assumption is that the relative permeability of the magnet is 1.05 (just a touch higher than the magnetic permeability of air). You can then specify the coercivity as:
% Hc = 155319 A/m * sqrt(BHmax/MGOe)
% So, for example if an N42 magnet were being modeled, the coercivity would be:
% Hc = 155319 A/m * sqrt(42) = 1006582 A/m