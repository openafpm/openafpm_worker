%%%%%%%%%%%%% environment setup %%%%%%%%%%%%%

%addpath('c:\femm42\mfiles'); % add FEMM mfiles to MATLAB
%addpath('~/.wine/drive_c/femm42/mfiles'); % add FEMM mfiles to MATLAB
%location_path=['/tmp/']; % path location to write files and read from

clear all % clear all previous variables in MATLAB
addpath('c:\femm42\mfiles'); % add FEMM mfiles to MATLAB
location_path=['C:\AFPM_mfiles\NTUA_wind_worker-master_20_2_18\UserAFPM\']; % path location to write files and read from

%%%%%%%%%% Input parameters  %%%%%%%%%%%%%
run_solver=true; % always true in UserAFPM
automesh=1; % (0) Use fine mesh (1) Automesh when running solver 

%%% New input variables added on 13_12_17 to make UserAFPM from MagnAFPM
Iac_nom=18; % use 'Estimation of the generator's current at rated power' (gen stator inputs)
Nc=43; % Number of turns Nc per coil (gen stator inputs)
dc=1.5; % Magnet wire diameter in mm from HP recipe dc 2@1.5->1@2.1213 (gen stator inputs)
No_wires_at_hand=2; % use 'Number of wires at hand' (gen stator inputs)
wc=21; % 'wc' coil leg width from HP recipe book (gen stator inputs)
Dout=300; % 'Dout' outer diameter from HP recipe book (gen rotor inputs)
tc_real=57.7; % coil operating temperature needs to match cq with when cq=0.3 then tc=106.5 deg
%%% End of new input variables

current_step=1; % step to increase current by - User defined with values 1,2,3,4 and 5
RPM_step=1; % step to increase rpm by - User defined with values 1,5,10 and 20

% displacement step in mm, choose from 1,2,3 etc - check angle in deg to be 1 degree or less
step=2;

% choose between single or double rotor topology
rotor=2; % double rotor (2) or single rotor with metal disk (1) or single rotor (0) 

% Choose stator winding type
coil_type=1; % choose (1) for rectangular coils or (2) for keyhole coils or (3) for triangular coils 

if coil_type==3
    winder_bolt_size=8; % coil winder bolt size at Rin for trianular coils (type 3)
end

% Radius to run simlation at
Radius_sim=1; % 1 for Ravg, 2 for Rin and 3 for Rout_eff

% magnetic material http://www.ndfeb-info.com/neodymium_grades.aspx
mag_mater='NdFeB N40'; % choose magnetic material between 'NdFeB N40/N42/N45' and 'Ferrite C8' 

% mechanical clearence + effective length + magnet size
g=3; % mechanical clearence gap including resin layers 0.5mm on coil and 0.5mm on magnet
la=46; % 'la' effective length and also magnet length
wm=30; % magnet width 

% blade rotor
Rturb=1.2; % turbine radius in meters
tsr_cut_in=8.75; % cut in tip speed ratio (HP 8.75 NTUA 9)
tsr_nom=4.4; % rated tip speed ratio 4.5 for batt, 6 or 7 for MPPT (HP uses 7)
Vw_cut_in=3; % cut in wind speed
Vw_nom=10; % rated-nominal windspeed

% battery or wind inverter cutin 
V_batt=24; % battery voltage or lower part of VDC range of wind inverter

% rated power
air_dens=1.204; % air density at 20 degrees
trans_loss=10; % Percent losses on cables at 10m/s
rect_loss=10; % Percent losses on rectifier

poles=12; % number of poles
hm=10; % magnet thickness 
tw=13; % 'tw' stator thickness
kf=0.55; % winding fill factor
hr=10; % 'hr' rotor disk thickness

% Copper
pcu=8.94; % copper density g/cm3
copperprice=12; % in Euro/kg
p20=1.678*10^(-8); % specific resistance of copper at 20 deg

% Iron
ironprice=1; % in Euro/kg
pFe=7.87; % iron density in g/cm3

% Resin
resinprice=11.23; % in Euro/kg
presin=1.36; % in g/cm3

% Wood (for moulds)
pplywood=17.6; % Euro/m2 for 12mm

% run sim
calc_gen_WindEng_15_12_17_UserAFPM

display 'Simulation complete'
