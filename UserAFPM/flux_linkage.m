% Radius to run simulation
if Radius_sim==1; % 1 for Ravg, 2 for Rin and 3 for Rout_eff
    Radius=Ravg;
elseif Radius_sim==2;
    Radius=Rin;
elseif Radius_sim==3;
    Radius=Rout_eff;
end

f=speed*poles/120; % calculate frequency
angle(j,d)=displacement/Radius; % mechanical angle in radians
angledeg(j,d)=angle(j,d)*360/(2*pi); % mechanical angle in degrees
time(j,d)=60*angle(j,d)/(2*pi*speed); % time from u=s/t

if j==1
    delta(j)=0;
else
    delta(j)=asin(Irms(j)*2*pi*f*Ls/Ef); % delta angle calculation
end

% Look here for 3phase voltages and rotor position http://www.youtube.com/watch?v=_jaIHJUf110
Ia(j,d)=-Irms(j)*sqrt(2)*sin(2*pi*f*time(j,d)-delta(j));%????
Ib(j,d)=-Irms(j)*sqrt(2)*sin(2*pi*f*time(j,d)+2*pi/3-delta(j));%??? DES peiramatika thn tash kai to reuma an bgazoyn ta idia plots me FEMM
Ic(j,d)=-Irms(j)*sqrt(2)*sin(2*pi*f*time(j,d)-2*pi/3-delta(j));%???
mi_modifycircprop('A',1,Ia(j,d)); % set currents in FEMM
mi_modifycircprop('B',1,Ib(j,d));
mi_modifycircprop('C',1,Ic(j,d));

mi_createmesh % create mesh in FEMM
mi_analyze(1) % analysis
mi_loadsolution % loads solution for post processor

vectorA=mo_getcircuitproperties('A'); % get circuit prop
vectorB=mo_getcircuitproperties('B');
vectorC=mo_getcircuitproperties('C');
fluxlinkageA(j,d)=vectorA(3); % return flux linkage
fluxlinkageB(j,d)=vectorB(3);
fluxlinkageC(j,d)=vectorC(3);
sumsquareA=sumsquareA+fluxlinkageA(j,d)^2; % sum for RMS flux
sumsquareB=sumsquareB+fluxlinkageB(j,d)^2;
sumsquareC=sumsquareC+fluxlinkageC(j,d)^2;
Va(j,d)=q*fluxlinkageA(j,d)*poles/2*2*pi*speed/60; % rms value of induced voltage in A
Vb(j,d)=q*fluxlinkageB(j,d)*poles/2*2*pi*speed/60; % rms value of induced voltage in B
Vc(j,d)=q*fluxlinkageC(j,d)*poles/2*2*pi*speed/60; % rms value of induced voltage in C

% calculates the magnitude of force

if rotor == 2
mo_addcontour(0,hr+g+tw+g/2); % draw contour for complete airgap length
mo_addcontour(L_half,hr+g+tw+g/2);

vector_Force=mo_lineintegral(3);
Force_x(j,d)=vector_Force(1);
Torque_x(j,d)=Force_x(j,d)*2*(Radius/1000)*q;

mo_clearcontour(); % clear contour 
mo_clearcontour();

end

if rotor == 1
mo_addcontour(0,hr+g+tw+g/2); % draw contour for complete top airgap length
mo_addcontour(L_half,hr+g+tw+g/2);

vector_Force=mo_lineintegral(3);
Force_x_top(j,d)=vector_Force(1);

mo_clearcontour(); % clear contour 
mo_clearcontour();

mo_addcontour(0,hr+g/2); % draw contour for complete bottom airgap length
mo_addcontour(L_half,hr+g/2);

vector_Force=mo_lineintegral(3);
Force_x_bot(j,d)=vector_Force(1);

mo_clearcontour(); % clear contour 
mo_clearcontour();

Torque_x(j,d)=(Force_x_top(j,d)+Force_x_bot(j,d))*(Radius/1000)*q;

end


if rotor == 0
mo_addcontour(0,extra+hr+g+tw+g/2); % draw contour for complete top airgap length
mo_addcontour(L_half,extra+hr+g+tw+g/2);

vector_Force=mo_lineintegral(3);
Force_x_top(j,d)=vector_Force(1);

mo_clearcontour(); % clear contour 
mo_clearcontour();

mo_addcontour(0,extra+hr+g/2); % draw contour for complete bottom airgap length
mo_addcontour(L_half,extra+hr+g/2);

vector_Force=mo_lineintegral(3);
Force_x_bot(j,d)=vector_Force(1);

mo_clearcontour(); % clear contour 
mo_clearcontour();

Torque_x(j,d)=(Force_x_top(j,d)+Force_x_bot(j,d))*(Radius/1000)*q;

end

