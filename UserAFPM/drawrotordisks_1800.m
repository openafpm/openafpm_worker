if rotor == 1 
    
% draw bottom rotor disk
mi_drawrectangle(0,0,L_half,hr);
% set properties for all segments of the first rotor disk and place in
% group 4
mi_selectsegment(0,1);
mi_setsegmentprop('',msize,0,0,4);
mi_clearselected
mi_selectsegment(L_half-1,hr);
mi_setsegmentprop('',msize,0,0,4);
mi_clearselected
mi_selectsegment(L_half-1,0);
mi_setsegmentprop('',msize,0,0,4);
mi_clearselected
mi_selectsegment(L_half,1);
mi_setsegmentprop('',msize,0,0,4);
mi_clearselected
% select group
mi_selectgroup(4);
% copy one more time in order to create other rotor disk
mi_copytranslate(0,hr+g+tw+g+hm,1);
mi_clearselected

elseif rotor == 2
    
% draw bottom rotor disk
mi_drawrectangle(0,0,L_half,(hr-hm));
% set properties for all segments of the first rotor disk and place in
% group 4
mi_selectsegment(0,1);
mi_setsegmentprop('',msize,0,0,4);
mi_clearselected
mi_selectsegment(L_half-1,hr-hm);
mi_setsegmentprop('',msize,0,0,4);
mi_clearselected
mi_selectsegment(L_half-1,0);
mi_setsegmentprop('',msize,0,0,4);
mi_clearselected
mi_selectsegment(L_half,1);
mi_setsegmentprop('',msize,0,0,4);
mi_clearselected
% select group
mi_selectgroup(4);
% copy one more time in order to create other rotor disk
mi_copytranslate(0,(hr-hm)+hm+g+tw+g+hm,1);
mi_clearselected

elseif rotor == 0

% draw bottom rotor disk
mi_drawrectangle(0,0,L_half,(hr-hm));
% set properties for all segments of the first rotor disk and place in
% group 4
mi_selectsegment(0,1);
mi_setsegmentprop('',msize,0,0,4);
mi_clearselected
mi_selectsegment(L_half-1,hr-hm);
mi_setsegmentprop('',msize,0,0,4);
mi_clearselected
mi_selectsegment(L_half-1,0);
mi_setsegmentprop('',msize,0,0,4);
mi_clearselected
mi_selectsegment(L_half,1);
mi_setsegmentprop('',msize,0,0,4);
mi_clearselected
% select group
mi_selectgroup(4);
% copy one more time in order to create other rotor disk
mi_copytranslate(0,(hr-hm)+hm+g+tw+g+hm,1);
mi_clearselected
%delete bottom rotor disk
mi_selectsegment(L_half/2,hr-hm);
mi_deleteselected

end