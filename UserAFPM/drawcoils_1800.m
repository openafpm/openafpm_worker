if rotor == 1 || rotor == 2

% draw first coil leg
mi_drawrectangle(coil_spacing/2,hr+g,wc+coil_spacing/2,hr+g+tw); 
% set properties for all segments of the first coil leg and place in group 5
mi_selectsegment(coil_spacing/2,hr+g+1);
mi_setsegmentprop('',msize,0,0,5);
mi_clearselected
mi_selectsegment(coil_spacing/2+wc-1,hr+g);
mi_setsegmentprop('',msize,0,0,5);
mi_clearselected
mi_selectsegment(coil_spacing/2+wc-1,hr+g+tw);
mi_setsegmentprop('',msize,0,0,5);
mi_clearselected
mi_selectsegment(coil_spacing/2+wc,hr+g+1);
mi_setsegmentprop('',msize,0,0,5);
mi_clearselected
% select the group
mi_selectgroup(5);
% copy in order to create the secoind leg of the coil
mi_copytranslate(coil_whole+wc,0,1);
mi_clearselected
mi_selectgroup(5);
% copy another 2 times in order to create one coil for each phase
mi_copytranslate(2*wc+coil_whole+coil_spacing,0,coil_FEMM-1);
mi_clearselected

elseif rotor == 0
    
% draw first coil leg
mi_drawrectangle(coil_spacing/2,extra+hr+g,wc+coil_spacing/2,extra+hr+g+tw); 
% set properties for all segments of the first coil leg and place in group 5
mi_selectsegment(coil_spacing/2,extra+hr+g+1);
mi_setsegmentprop('',msize,0,0,5);
mi_clearselected
mi_selectsegment(coil_spacing/2+wc-1,extra+hr+g);
mi_setsegmentprop('',msize,0,0,5);
mi_clearselected
mi_selectsegment(coil_spacing/2+wc-1,extra+hr+g+tw);
mi_setsegmentprop('',msize,0,0,5);
mi_clearselected
mi_selectsegment(coil_spacing/2+wc,extra+hr+g+1);
mi_setsegmentprop('',msize,0,0,5);
mi_clearselected
% select the group
mi_selectgroup(5);
% copy in order to create the secoind leg of the coil
mi_copytranslate(coil_whole+wc,0,1);
mi_clearselected
mi_selectgroup(5);
% copy another 2 times in order to create one coil for each phase
mi_copytranslate(2*wc+coil_whole+coil_spacing,0,coil_FEMM-1);
mi_clearselected

end