extra=40; % extra space in the case of rotor (0) in order to allow for magnetic field to expand

 if strcmp(mag_mater,'Ceramic 8')
     Br=0.39; % T
     Hc=227950; % A/m from FEMM library
     BHmax=30; % KJ/m3
     mag_density=5; % density of Ferrite in g/cm3
 end

 if strcmp(mag_mater,'NdFeB N35')
     Br=1.195; % T
     Hc=155319*sqrt(35); % A/m from FEMM library
     BHmax=287; % KJ/m3
     mag_density=7.5; % density of NdFeB in g/cm3
 end

 if strcmp(mag_mater,'NdFeB N40')
     Br=1.265; % T
     Hc=155319*sqrt(40); % A/m from FEMM library
     BHmax=326; % KJ/m3
     mag_density=7.5; % density of NdFeB in g/cm3
 end

 if strcmp(mag_mater,'NdFeB N42')
     Br=1.300; % T
     Hc=155319*sqrt(42); % A/m from FEMM library
     BHmax=342; % KJ/m3
     mag_density=7.5; % density of NdFeB in g/cm3
 end

 if strcmp(mag_mater,'NdFeB N45')
     Br=1.350; % T
     Hc=155319*sqrt(45); % A/m from FEMM library
     BHmax=366; % KJ/m3
     mag_density=7.5; % density of NdFeB in g/cm3
 end

 if strcmp(mag_mater,'NdFeB N52')
     Br=1.450; % T
     Hc=155319*sqrt(52); % A/m from FEMM library
     BHmax=422; % KJ/m3
     mag_density=7.5; % density of NdFeB in g/cm3
 end

% blade rotor
n_cut_in=(60*Vw_cut_in*tsr_cut_in)/(2*3.14*Rturb); % cut in rpm

n_nom=(60*Vw_nom*tsr_nom)/(2*3.14*Rturb); % rated rpm

% EMf at cut-in
EMF_cut_in=V_batt/(sqrt(3)*1.35); % EMF at cut-in (not including voltage drop 1.4V at rectifier)

% EMF at rated windspeed
EMF_nom=(n_nom/n_cut_in)*EMF_cut_in;

fnom=poles*n_nom/120; % nominal frequency

% coils
coil_num=poles*0.75; % 3 coils to 4 magnets
nphase=3; % number of phases
q=coil_num/nphase; % Number of coils in phase group for 3 phase system
coil_FEMM=coil_num/q; % FEMM will use part of the stator according to phase coil number
magnet_num=coil_num/0.75; % number of rotor magnets, 3 coils to 4 magnets
magnet_FEMM=magnet_num/q; % FEMM will use only the part of the rotor that covers three coils

sc=No_wires_at_hand*(pi()*(dc/2)^2); % calculate crossectional area
dc_theor=2*sqrt(sc/pi()); % calculate dimaeter of total wire

Jmax_real=Iac_nom/sc; % maximum current desinty

% For triangular coils calculate Ravg. For rectangular coils calculate Rin.
% It is assumed that the coil hole at Ravg and Rin is equal to the magnet
% width wm for triangular and rectangular coils respectively.

if coil_type==2 % for double layer concentrated winding (triangular coil)
    coil_side_dist=0.5; % Space between coils at Ravg
    Rout=Dout/2;
    % calculate segment due to magnet placed as a chord on the disk
    rts=roots([1 -2*Rout +(wm/2)^2]); % Rout=segment+la+Rin
    for i=1:2
        if rts(i,1)>0
            segment=rts(i,1);
        end
    end
    Ravg=Rout-segment-la/2; % Add 1% to radius for triangular coils so that they fit
    L_half=round_2dec(2*(pi)*Ravg/q); % Part of the perimeter at Ravg to be drawn in FEMM acccording to number of coils
    Rin=Ravg-la/2; % Rin is Ravg minus half the effective length la
    Rout_eff=Ravg+la/2; % The outer radius of the effective length of the generator
end

if coil_type==3 % F series HP triangular coils
    coil_side_dist=0.5; % Space between coils at Ravg
    Rout=Dout/2;
    % calculate segment due to magnet placed as a chord on the disk
    rts=roots([1 -2*Rout +(wm/2)^2]); % Rout=segment+la+Rin
    for i=1:2
        if rts(i,1)>0
            segment=rts(i,1);
        end
    end
    Ravg=Rout-segment-la/2; % Add 1% to radius for triangular coils so that they fit
    L_half=round_2dec(2*(pi)*Ravg/q); % Part of the perimeter at Ravg to be drawn in FEMM acccording to number of coils
    Rin=Ravg-la/2; % Rin is Ravg minus half the effective length la
    Rout_eff=Ravg+la/2; % The outer radius of the effective length of the generator
end

if coil_type==1 % for single layer concentrated winding (rectangular coil)
    coil_side_dist=0.5; % Space between coils at Ravg
    Rout=Dout/2;
    % calculate segment due to magnet placed as a chord on the disk
    rts=roots([1 -2*Rout +(wm/2)^2]); % Rout=segment+la+Rin
    for i=1:2
        if rts(i,1)>0
            segment=rts(i,1);
        end
    end
    Rin=Rout-la-segment;
    Rout_eff=Rout-segment; % The outer radius of the effective length of the generator
    Ravg=round_2dec(Rout_eff-la/2); % 'Ravg' radius in the middle of the effective length
    L_half=round_2dec(2*(pi)*Ravg/q); % Part of the perimeter at Ravg to be drawn in FEMM acccording to number of coils
end

% Back iron disk mass
BackIronVolume=2*hr*pi*(Rout^2)*10^(-3);% 2 rotor disks volume in cm3 / add -0.4*2*hr*pi*(Rin^2)*10^(-3); if whole in back iron disk. In this case 40% less material.
BackIronMass=BackIronVolume*pFe*10^(-3); % 2 rotor disks mass in kgr

if rotor == 0
    BackIronVolume=hr*pi*(Rout^2)*10^(-3);% 1 rotor disks volume in cm3 / add -0.4*2*hr*pi*(Rin^2)*10^(-3); if whole in back iron disk. In this case 40% less material.
    BackIronMass=BackIronVolume*pFe*10^(-3); % 1 rotor disks mass in kgr
end

% kd
kd=Rin/Rout_eff; % inner to outer radius ratio

%Design coils
if coil_type==2 % for double layer concentrated winding (triangular coil)
    coil_spacing=coil_side_dist; % make these variables the same
    coil_whole=wm;%(round_2dec((2*(pi)*Ravg-((2*wc+coil_side_dist)*coil_num))/coil_num)); % length of coil whole at Ravg is wm
    thitam=pi()*poles/coil_num; % calculation for lavg of coil
    thitare=kd/(1+kd)*thitam;
    lec=2*(Rout_eff+Rin)*(thitam-0.6*thitare)/poles;
    lavg=2*la+lec; % include kf in these calculation somehow??????
    Kn=(1+0.9*lavg/(2*pi*tw)+0.32*2*pi*wc/lavg+0.84*wc/tw)^(-1); % the Nagaoka constant for the calculation of phase inductance
    Ls=(q*((lavg/1000)^2)*(Nc^2)*(10^(-7))/(tw/1000))*Kn; % phase inductance
    coil_hole_Ravg=(2*pi*Ravg-2*coil_num*wc-coil_side_dist*coil_num)/coil_num; % coil whole dimensions (arc)
    coil_hole_Rin=(2*pi*Rin-2*coil_num*wc-coil_side_dist*coil_num)/coil_num; % coil whole dimensions (arc)
    coilangle_Rin=360/(2*pi*Rin/coil_hole_Rin);
    coil_hole_Rin_constr=2*Rin*sin((coilangle_Rin*pi/180)/2);% coil whole dimensions (line)
    coil_hole_Rout=(2*pi*Rout_eff-2*coil_num*wc-coil_side_dist*coil_num)/coil_num; % coil whole dimensions (arc)
    coilangle_Rout=360/(2*pi*Rout_eff/coil_hole_Rout);
    coil_hole_Rout_constr=2*Rout_eff*sin((coilangle_Rout*pi/180)/2);% coil whole dimensions (line)
    if Radius_sim==2
        coil_whole_radius_sim=round_2dec(coil_hole_Rin);
        coil_whole=coil_whole_radius_sim;
    elseif Radius_sim==3
        coil_whole_radius_sim=round_2dec(coil_hole_Rout);
        coil_whole=coil_whole_radius_sim;
    elseif Radius_sim==1
        coil_whole_radius_sim=round_2dec(coil_hole_Ravg);
        coil_whole=coil_whole_radius_sim;
    else
    end
end

if coil_type==3 % F series HP triangular coils
    coil_whole=(wm/2+winder_bolt_size/2); %round_2dec((2*(pi)*Ravg-((2*wc+coil_side_dist)*coil_num))/coil_num); % length of coil whole at Ravg is wm
    coil_spacing=round_2dec((2*(pi)*Ravg-((2*wc+coil_whole)*coil_num))/coil_num); % Space between coils at Ravg
    thitam=pi*poles/coil_num; % calculation for lavg of coil
    thitare=kd/(1+kd)*thitam;
    lec=2*(Rout_eff+Rin)*(thitam-0.6*thitare)/poles;
    lavg=2*la+lec; % include kf in these calculation somehow??????
    Kn=(1+0.9*lavg/(2*pi*tw)+0.32*2*pi*wc/lavg+0.84*wc/tw)^(-1); % the Nagaoka constant for the calculation of phase inductance
    Ls=(q*((lavg/1000)^2)*(Nc^2)*(10^(-7))/(tw/1000))*Kn; % phase inductance
    coil_hole_Rin_constr=winder_bolt_size; %(2*pi*Rin-2*coil_num*wc-coil_side_dist*coil_num)/coil_num; % coil whole dimensions (arc)
    %coilangle_Rin=360/(2*pi*Rin/coil_hole_Rin);
    %coil_hole_Rin_constr=2*Rin*sin((coilangle_Rin*pi/180)/2);% coil whole dimensions (line)
    %coil_hole_Rout=(2*pi*(Rout_eff/1.06)-2*coil_num*wc-coil_side_dist*coil_num)/coil_num; % coil whole dimensions (arc)
    %coilangle_Rout=360/(2*pi*Rout_eff/coil_hole_Rout);
    coil_hole_Rout_constr=wm; %2*Rout_eff*sin((coilangle_Rout*pi/180)/2);% coil whole dimensions (line)
    if Radius_sim==2
        coil_whole_radius_sim=round_2dec(coil_hole_Rin_constr);
        coil_whole=coil_whole_radius_sim;
    elseif Radius_sim==3
        coil_whole_radius_sim=round_2dec(coil_hole_Rout_constr);
        coil_whole=coil_whole_radius_sim;
    elseif Radius_sim==1
        coil_whole_radius_sim=coil_whole;
        coil_whole=coil_whole_radius_sim;
    else
    end
end

if coil_type==1 % for single layer concentrated winding (rectangular coil)
    coil_whole=wm; % length of coil whole at Ravg is wm
    coil_hole_Rin_constr=wm;
    coil_hole_Rout_constr=wm;
    coil_spacing=round_2dec((2*(pi)*Ravg-((2*wc+coil_whole)*coil_num))/coil_num); % Space between coils at Ravg
    lavg=2*(wm+la)+3.14*wc; % average length of a turn or wire in coil, from HP recipe book p56
    Kn=(1+0.9*lavg/(2*pi*tw)+0.32*2*pi*wc/lavg+0.84*wc/tw)^(-1); % NOT GOOD FOR THIS TYRP OF COIL the Nagaoka constant for the calculation of phase inductance
    Ls=(q*((lavg/1000)^2)*(Nc^2)*(10^(-7))/(tw/1000))*Kn; % NOT GOOD phase inductance
    if Radius_sim==2
        coil_whole_radius_sim=coil_whole;
    elseif Radius_sim==3
        coil_whole_radius_sim=coil_whole;
    elseif Radius_sim==1
        coil_whole_radius_sim=coil_whole;
    else
    end
end

% Radius to run simulation
if coil_type==1
    if Radius_sim==1
        L_half=round_2dec(2*(pi)*Ravg/q); % Part of the perimeter at Ravg to be drawn in FEMM acccording to number of coils
        dist_magnet=round_2dec((2*pi*Ravg-magnet_num*wm)/magnet_num); % Distance between magnets at Ravg
        coil_spacing=round_2dec((2*(pi)*Ravg-((2*wc+coil_whole)*coil_num))/coil_num); % Space between coils at Ravg
    elseif Radius_sim==2
        Rin=Rout-la+0.25; % recalculate inner radius
        L_half=round_2dec(2*(pi)*Rin/q); % Part of the perimeter at Rin to be drawn in FEMM acccording to number of coils
        dist_magnet=round_2dec((2*pi*Rin-magnet_num*wm)/magnet_num); % Distance between magnets at Rin
        coil_spacing=round_2dec((2*(pi)*Rin-((2*wc+coil_whole)*coil_num))/coil_num); % Space between coils at Rin
    elseif Radius_sim==3
        L_half=round_2dec(2*(pi)*Rout_eff/q); % Part of the perimeter at Rout_eff to be drawn in FEMM acccording to number of coils
        dist_magnet=round_2dec((2*pi*Rout_eff-magnet_num*wm)/magnet_num); % Distance between magnets at Rout_eff
        coil_spacing=round_2dec((2*(pi)*Rout_eff-((2*wc+coil_whole)*coil_num))/coil_num); % Space between coils at Rout_eff
    else
    end
end
  
if coil_type==2
    if Radius_sim==1
        L_half=round_2dec(2*(pi)*Ravg/q); % Part of the perimeter at Ravg to be drawn in FEMM acccording to number of coils
        dist_magnet=round_2dec((2*pi*Ravg-magnet_num*wm)/magnet_num); % Distance between magnets at Ravg
        coil_spacing=round_2dec((2*(pi)*Ravg-((2*wc+coil_whole_radius_sim)*coil_num))/coil_num); % Space between coils at Ravg
    elseif Radius_sim==2
        Rin=Rout-la+1; % recalculate inner radius
        L_half=round_2dec(2*(pi)*Rin/q); % Part of the perimeter at Rin to be drawn in FEMM acccording to number of coils
        dist_magnet=round_2dec((2*pi*Rin-magnet_num*wm)/magnet_num); % Distance between magnets at Rin
        coil_spacing=round_2dec((2*(pi)*Rin-((2*wc+coil_whole_radius_sim)*coil_num))/coil_num); % Space between coils at Rin
    elseif Radius_sim==3
        L_half=round_2dec(2*(pi)*Rout_eff/q); % Part of the perimeter at Rout_eff to be drawn in FEMM acccording to number of coils
        dist_magnet=round_2dec((2*pi*Rout_eff-magnet_num*wm)/magnet_num); % Distance between magnets at Rout_eff
        coil_spacing=round_2dec((2*(pi)*Rout_eff-((2*wc+coil_whole_radius_sim)*coil_num))/coil_num); % Space between coils at Rout_eff
    else
    end
end

if coil_type==3
    if Radius_sim==1
        L_half=round_2dec(2*(pi)*Ravg/q); % Part of the perimeter at Ravg to be drawn in FEMM acccording to number of coils
        dist_magnet=round_2dec((2*pi*Ravg-magnet_num*wm)/magnet_num); % Distance between magnets at Ravg
        coil_spacing=round_2dec((2*(pi)*Ravg-((2*wc+coil_whole_radius_sim)*coil_num))/coil_num); % Space between coils at Ravg
    elseif Radius_sim==2
        L_half=round_2dec(2*(pi)*Rin/q); % Part of the perimeter at Rin to be drawn in FEMM acccording to number of coils
        dist_magnet=round_2dec((2*pi*Rin-magnet_num*wm)/magnet_num); % Distance between magnets at Rin
        coil_spacing=round_2dec((2*(pi)*Rin-((2*wc+coil_whole_radius_sim)*coil_num))/coil_num); % Space between coils at Rin
    elseif Radius_sim==3
        L_half=round_2dec(2*(pi)*Rout_eff/q); % Part of the perimeter at Rout_eff to be drawn in FEMM acccording to number of coils
        dist_magnet=round_2dec((2*pi*Rout_eff-magnet_num*wm)/magnet_num); % Distance between magnets at Rout_eff
        coil_spacing=round_2dec((2*(pi)*Rout_eff-((2*wc+coil_whole_radius_sim)*coil_num))/coil_num); % Space between coils at Rout_eff
    else
    end
end

% ai 
dist_magnet_Rin=round_2dec((2*pi*Rin-magnet_num*wm)/magnet_num); % Distance between magnets at Rin
ai=wm/(wm+dist_magnet); % pole arc to pole pitch (magnet width) ratio

pt=p20*(1+0.0039*(tc_real-20)); % specific resistance of copper at operating temp
cq_real=(((Iac_nom*Nc/wc)^2)*(pt/(2*kf*tw)))*10^5; % cq heat coefficient w/cm2

% calculation of coil resitance at operating temp
Rc=pt*Nc*lavg*10^(-3)/(sc*10^(-6)); % coil resistance
Rphase=Rc*q; % phase resistance

stator_surface_area=2*q*nphase*(wc*lavg)*(10^-2); % coil surface area in cm2

%Resistance calculation HP style!
lavgHP=lavg;
length__wire_HP=lavgHP*(Nc+2); % in mm + add 2 turns about 20cm each due to connection wire of coil at the two edges
ResHP_20=(length__wire_HP)/sc/56000; % in ohm at 20 Celsius
ResHP_70=ResHP_20*1.25; % at 70 Celsius
coil_weight_constr=sc*length__wire_HP*0.00894; % coil weight g
mcu_constr=(coil_weight_constr*(coil_num+1))/1000; % stator weight kgr + 1 extra coils

% Total magnet mass
VolumeMagnet=la*wm*hm*magnet_num*2; % Total magnet volume of 2 rotors in mm3
MagnetMass=mag_density*VolumeMagnet*10^(-3); % Magnet mass in g

% Hub mass
if Rturb<1.7
    No_bolts_hub=4;
    hub_radius=10; % bearing hub radius in cm
    hub_thickness=1.5; % bearing hub thickness in cm
elseif Rturb>=1.7 && Rturb<2.7
    No_bolts_hub=5;
    hub_radius=15; % bearing hub radius in cm
    hub_thickness=1.5; % bearing hub thickness in cm
elseif Rturb>=2.7 && Rturb<3.7
    No_bolts_hub=6;
    hub_radius=17; % bearing hub radius in cm
    hub_thickness=2; % bearing hub thickness in cm
elseif Rturb>=3.7 && Rturb<=5
    No_bolts_hub=8;
    hub_radius=20; % bearing hub radius in cm
    hub_thickness=3; % bearing hub thickness in cm
else
end
BearingHubMass=pi*hub_radius^2*hub_thickness*pFe; % Rotational mass of bearing hub in g (length and radius in cm)

% Blade rotor mass in kgr
blade_rotor_volume=3*(Rturb*(Rturb/29.5)*(Rturb/9.5)/2.8); % in m3 - 0.00239m3 from 20005 blade scan - from recipe book lade lenght to windth 9.5 and blade length to thickness 29.5 and also 2005 blade volume
softwood_density=500; % for pine about 500kgr/m3
blade_rotor_mass=blade_rotor_volume*softwood_density; % in kgr

% Calculate toal cost of generator

% Magnet cost as of January 2018
Vmag=wm*la*hm;
shippingcost=Vmag*2*poles*20/(24*13800);
 if strcmp(mag_mater,'Ceramic 8')
    Magcost=(0.4105+10^(-5)*Vmag)*2*poles+shippingcost;
 elseif strcmp(mag_mater,'NdFeB N35')
    Magcost=(0.5795*Vmag*10^(-3)+0.3967)*2*poles+shippingcost;
 elseif strcmp(mag_mater,'NdFeB N40')
    Magcost=(0.5995*Vmag*10^(-3)+0.5)*2*poles+shippingcost;
 elseif strcmp(mag_mater,'NdFeB N42')
    Magcost=(0.6185*Vmag*10^(-3)+0.536)*2*poles+shippingcost;
 elseif strcmp(mag_mater,'NdFeB N45')
    Magcost=(0.6751*Vmag*10^(-3)+0.27038)*2*poles+shippingcost;
 elseif strcmp(mag_mater,'NdFeB N52')
    Magcost=(0.7971*Vmag*10^(-3)+0.0552)*2*poles+shippingcost;
 end

% Copper cost
mcu=3*Nc*q*lavg*sc*pcu*(10^(-6)); % in kg
coppercost=1.23*copperprice*mcu;

% Iron cost
ironcost=1.23*ironprice*BackIronMass;

% Resin cost
coil_volume=wc*lavg*tw; % coil volume in mm3
ResinVolume=(2*(pi*((Rout+5)^2-(Rin-20)^2)*tw*10^(-3)))-(2*poles*Vmag*10^(-3))+((pi*((Rout+wc+10)^2-(Rin-wc)^2)*tw*10^(-3)))-(coil_num*coil_volume*10^(-3)); % in cm3
resinmass=presin*ResinVolume*10^(-3);% in kg
resincost=1.23*resinmass*resinprice;

% Wood cost (for moulds)
woodS=(2*(Rout*10^(-3)+0.15))^2; % Surface area of square moulds
woodcost=3*woodS*pplywood+6*woodS*pplywood; % Euro

%Total cost of generator
Totalcost=Magcost+coppercost+ironcost+resincost+woodcost;

%Total mass of generator
TotalMass=mcu+resinmass+BackIronMass+MagnetMass/1000;

%Total mass of 1 rotor
TotalMassRotor=(BackIronMass+MagnetMass/1000)/2;

if rotor == 0
    TotalMassRotor=(BackIronMass+MagnetMass/1000);
end

%Total volume
if rotor == 2
    TotalVolume= pi*Rout^2*(hr+hm+g+tw+g+hm+hr)/10^6;
end

if rotor == 1
    TotalVolume= pi*Rout^2*(hr+g+tw+g+hm+hr)/10^6;
end

if rotor == 0
    TotalVolume= pi*Rout^2*(tw+g+hm+hr)/10^6;
end

% Construction limitations
flag_solver=0; % Decide when to run solver and when not to
if ((2*pi*Rin-magnet_num*wm)/magnet_num)<0
    display('Construction limitation: It is not possible to build this magnet rotor because the magnets are overlapping at the inner radius of the generator. Try reducing the number of poles.')
    flag_solver=1;
end

if coil_spacing<coil_side_dist
    display('Construction limitation: It is not possible to build this stator because the coils are overlapping at the simulation radius of the generator. Try increasing the number of poles.')
    flag_solver=1;
end

if (coil_hole_Rin_constr-5)<=0
    display('Construction limitation: It is not possible to build this coil because its whole at the inner radius is less than 5mm. Try reducing the number of poles.')
    flag_solver=1;
end

if (tw/wc)<=0.25 % from 0.35
    display('Construction limitation: It is not possible to build this stator because the leg width of the coils is more than three times the thickness of the stator. Try increasing the number of poles or the value of the heat coefficient.')
    flag_solver=1;
end

if (tw/wc)>2.5 % from 2
    display('Construction limitation: It is not possible to build this stator because the thickness of the coils is more than twice the leg width of the coils. Try reducing the number of poles.')
    flag_solver=1;
end

% Calculate back iron disk thickness for saturation at 1.5T
if flag_solver==0
    Bp_ai_kd_WindEng_18_12_16 % draw the rotor to calculate Bp and Bmg
    CALC_hr_saturation;
    % Flux per pole according to wind engineering paper
    % (equations that do not take into account changes in ai kd)
    % these are from AFPM book
    Flux_pole=ai*Bmg*(3.14/(8*poles/2))*((Rout*2*0.001)^2)*(1-kd^2);
else
end


% rotational speed
speed=n_nom;

% maximum current to simulate and current step, set to zero for no load operation
Imax=Iac_nom;
I_step=Iac_nom/current_step; % User defined with values 1,2,3,4 and 5

if flag_solver==0 && run_solver==true
    % open solver to run FEMM simulation
    display 'Running solver';
    solver;
    % find cut-in RPM
    RPMcutinCALC;
    % run performance
    display 'Running performance';
    performance_15_12_17_UserAFPM;

    if efftotalN_Torque_x<0.70
    display('Warning: The efficiency of the generator is below 70% and it is advisable not to reduce it further. Look carefully at your design. Consider increasing the number of poles or increasing the copper wire diameter.')
    end
     % get a screan shot of FEMM output and flux density
    mo_showdensityplot(1,0,0,2,'mag');
    path=[location_path,'FEMM_screenshot.bmp'];
    mo_savebitmap(path);
else 
end

