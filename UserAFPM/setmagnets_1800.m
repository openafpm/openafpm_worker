% set magnet block properties

% place in the middle of magnet
mi_addblocklabel(dist_magnet/2+wm/2,hr+g+tw+g+hm/2); 
mi_selectlabel(dist_magnet/2+wm/2,hr+g+tw+g+hm/2);
mi_setblockprop(mag_mater,automesh,msize,'',90,3,0);
% copy to magnets of rotor with same magnetic field direction
mi_copytranslate(2*(wm+dist_magnet),0,magnet_FEMM/2-1);
% repeat for the rest of the magnets that have opposiite magnetic field
% direction
mi_addblocklabel(dist_magnet/2+wm/2+wm+dist_magnet,hr+g+tw+g+hm/2);
mi_selectlabel(dist_magnet/2+wm/2+wm+dist_magnet,hr+g+tw+g+hm/2);
mi_setblockprop(mag_mater,automesh,msize,'',-90,3,0);
mi_copytranslate(2*(wm+dist_magnet),automesh,magnet_FEMM/2-1);

if rotor == 2
    
% set magnet block properties for bottom rotor disk

% place in the middle of magnet
mi_selectlabel(dist_magnet/2+wm/2,hr+g+tw+g+hm/2);
% copy to magnets of rotor with same magnetic field direction
mi_copytranslate(0,-(hm/2+g+tw+g+hm/2),1);
mi_selectlabel(dist_magnet/2+wm/2,hr+g+tw+g+hm/2);
mi_copytranslate(2*(wm+dist_magnet),-(hm/2+g+tw+g+hm/2),1);
% repeat for the rest of the magnets that have opposiite magnetic field
% direction
mi_selectlabel(dist_magnet/2+wm/2+wm+dist_magnet,hr+g+tw+g+hm/2);
mi_copytranslate(0,-(hm/2+g+tw+g+hm/2),1);
mi_selectlabel(dist_magnet/2+wm/2+wm+dist_magnet,hr+g+tw+g+hm/2);
mi_copytranslate(2*(wm+dist_magnet),-(hm/2+g+tw+g+hm/2),1);

end