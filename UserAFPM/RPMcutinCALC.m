clear n freqN Ef deltaN deltaNdeg VtN PelN PcopperN Peddy PHMN ProtN PmechmeanN Torque_x_avg THMN TmechmeanN Trot effHMN efftotalN Vdc Idc Pdc

%Fluxoc=FluxRms(1); % do not take into account chages in flux linkage due to currents
Fluxoc=FluxRms; % do take into account chages in flux linkage due to currents

N_max=n_nom; % maximum rpm to run
N_min=0; % minimum rpm to run
N_step=n_nom/1000; % step to increase rpm by - User defined with values 1,5,10 and 20

for i=1
    n(1)=N_min; % start from minimum rpm
    for j=1:(N_max/N_step+1)
        Fluxoc=FluxRms(i); % in order to change flux linkage with current
        Ef(i,j)=q*Fluxoc*(poles/2)*2*pi*n(j)/60; % calculate EMF from 
        Vdc(i,j)=Ef(i,j)*sqrt(3)*1.35; % DC voltage after rectifier for Open circuit voltages
        if Vdc(i,j)>V_batt % find cut-in RPM
            cutin_RPM=round(n(j));
            cutin_Voltage=round(Vdc(i,j));
            cutin_Vw=(cutin_RPM*2*3.14*Rturb)/(60*tsr_cut_in); % cut in rpm
            break
        end
        if j~=(N_max/N_step+1) % stop at final value of rpm or add rpm step
            n(j+1)=n(j)+N_step;
        end
    end
end

